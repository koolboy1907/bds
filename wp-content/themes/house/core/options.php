<?php
/**
 * Created by PhpStorm.
 * User: vinh.nd
 * Date: 7/21/2017
 * Time: 8:49 AM
 */

if (!class_exists('Real_State_Theme_Options')) {

    class Real_State_Theme_Options
    {
        // Variables are in the Redux Framework
        public $args = array();
        public $sections = array();
        public $theme;
        public $ReduxFramework;

        // Load Redux Framework
        public function __construct()
        {
            if (!class_exists('ReduxFramework')) {
                return;
            }

            if (true == Redux_Helpers::isTheme(__FILE__)) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }
        }

        public function initSettings()
        {
            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        public function setSections()
        {
            $this->sections[] = [
                'title' => __('Header', 'bds'),
                'desc' => __('All of settings for header on this theme', 'bds'),
                'icon' => 'el el-chevron-right',
                'fields' => [
                    [
                        'id' => 'bds-logo-on',
                        'type' => 'switch',
                        'title' => __('Enable Image Logo', 'bds'),
                        'compiler' => 'bool',
                        'desc' => __('Do you want to change show image logo ?', 'bds'),

                    ],
                    [
                        'id' => 'bds-logo-image',
                        'type' => 'media',
                        'title' => __('Logo Image', 'bds'),
                        'desc' => __('Dimension 250 x 65. Allow file type *jpg, *png, *gif', 'bds'),
                    ],
                    [
                        'id' => 'bds-logo-mobile',
                        'type' => 'media',
                        'title' => __('Logo Mobile', 'bds'),
                        'desc' => __('Dimension 195 x 40. Allow file type *jpg, *png, *gif', 'bds'),
                    ],
                    [
                        'id' => 'bds-banner-top',
                        'type' => 'media',
                        'title' => __('Banner Top', 'bds'),
                        'desc' => __('Dimension 717 x 75. Allow file type *jpg, *png, *gif', 'bds'),
                    ]
                ]
            ];
            $this->sections[] = [
                'title' => __('Footer', 'bds'),
                'desc' => __('All of settings for footer on this theme', 'bds'),
                'icon' => 'el el-chevron-right',
                'fields' => [
                    [
                        'id' => 'bds-text-footer',
                        'type' => 'editor',
                        'title' => __('Info', 'bds'),
                        'desc' => __('The information needed for the footer', 'bds'),
                    ],
                    [
                        'id' => 'bds-bottom-footer',
                        'type' => 'text',
                        'title' => __('More info', 'bds'),
                        'desc' => __('The information needed for the footer bottom', 'bds'),
                    ],
                    [
                        'id' => 'bds-footer-image',
                        'type' => 'media',
                        'title' => __('Footer Image', 'bds'),
                        'desc' => __('Dimension 180 x 68. Allow file type *jpg, *png, *gif', 'bds'),
                    ],
                    [
                        'id' => 'bds-link-img-footer',
                        'type' => 'text',
                        'title' => __('Link Image Footer', 'bds'),
                        'desc' => __('The link needed for the image footer', 'bds'),
                    ],
                ]
            ];
            $this->sections[] = [
                'title' => __('Section Top', 'bds'),
                'desc' => __('All of settings for section top on this theme', 'bds'),
                'icon' => 'el el-chevron-right',
                'fields' => [
                    [
                        'id' => 'bds-title-sectionTop',
                        'type' => 'text',
                        'title' => __('Title', 'bds'),
                        'desc' => __('The title needed for the section', 'bds'),
                    ],
                    [
                        'id' => 'bds-link-title',
                        'type' => 'text',
                        'title' => __('Link Title', 'bds'),
                        'desc' => __('The link needed for the title', 'bds'),
                    ],
                    [
                        'id' => 'bds-info-sectionTop',
                        'type' => 'editor',
                        'title' => __('Information', 'bds'),
                        'desc' => __('The information needed for the section top', 'bds'),
                    ],
                    [
                        'id' => 'bds-sectionTop-banner',
                        'type' => 'media',
                        'title' => __('Banner', 'bds'),
                        'desc' => __('Dimension 300 x 200. Allow file type *jpg, *png, *gif', 'bds'),
                    ],
                ]
            ];
        }

        public function setArguments()
        {
            $theme = wp_get_theme();
            $title = __('Page Info', 'bds');
            $this->args = array(
                // Settings for theme options
                'opt_name' => 'bds_options',
                'display_name' => $theme->get('Name'),
                'menu_type' => 'menu',
                'allow_sub_menu' => true,
                'menu_title' => __('Page Info', 'bds'),
                'page_title' => __('Page Info', 'bds'),
                'dev_mode' => false,
                'customizer' => true,
                'menu_icon' => '',

                'hints' => array(
                    'icon' => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color' => 'lightgray',
                    'icon_size' => 'normal',
                    'tip_style' => array(
                        'color' => 'light',
                        'shadow' => true,
                        'rounded' => false,
                        'style' => '',
                    ),
                    'tip_position' => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect' => array(
                        'show' => array(
                            'effect' => 'slide',
                            'duration' => '500',
                            'event' => 'mouseover',
                        ),
                        'hide' => array(
                            'effect' => 'slide',
                            'duration' => '500',
                            'event' => 'click mouseleave',
                        ),
                    ),
                )
            );
        }

        public function setHelpTabs()
        {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id' => 'redux-help-tab-1',
                'title' => __('Theme Information 1', 'bds'),
                'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'bds')
            );

            $this->args['help_tabs'][] = array(
                'id' => 'redux-help-tab-2',
                'title' => __('Theme Information 2', 'bds'),
                'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'bds')
            );

            // Set the help sidebar
            $this->args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'bds');
        }
    }

    global $reduxConfig;
    $reduxConfig = new Real_State_Theme_Options();
}
