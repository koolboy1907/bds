<?php
/**
 * Created by PhpStorm.
 * User: vinh.nd
 * Date: 7/21/2017
 * Time: 8:33 AM
 */
function real_state_plugin_activation()
{
    // Declare plugin need settings
    $plugins = [
        [
            'name' => 'Redux Framework',
            'slug' => 'redux-framework',
            'required' => true
        ],
        [
            'name' => 'Real State User Manager',
            'slug' => 'real-state-user-manager',
            'required' => true
        ],
    ];

    // Settings TGM
    $configs = [
        'menu' => 'nd_plugin_install',
        'has_notice' => true,
        'dismissable' => false,
        'is_automatic' => true
    ];

    tgmpa($plugins, $configs);
}

add_action('tgmpa_register', 'real_state_plugin_activation');
