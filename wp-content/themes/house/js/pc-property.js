﻿//command
var COMMAND_GET_DISTRICTS_UL_FORMAT = 5;
var COMMAND_SEND_REFLECT_FORM = 25;
var COMMAND_GET_PROPERTY_LIST_NAVIGATION = 26;
$(document).ready(function() {
    InitRentNotice();
    /*select*/
    $(".search-box .text").click(function() {
        $(this).parent().children(".view").css("display", "block");
    });

    $(".search-box .view li").click(function() {
        $(this).parent().parent().parent().parent().children(".text").html($(this).html());
        $(this).parent().parent().parent().parent().children(".text").attr('name', $(this).attr('name')); //ex
        $(this).parent().parent().parent().css("display", "none");
    });

    //register event to new elements from ajax
    $("body").on('click', '.search-box .district-container .view li', function(event) {
        var district = $(this).attr('name');
        var newDistrict = $('.search-box .district-container .text').attr('name');
        if (district != $('.search-box .district-container .text').attr('name')) {
            $('.hddWard').val("0");
            $('.hddStreet').val("0");
            $('.search-box .location').val("");
        }

        $(this).parent().parent().parent().parent().children(".text").html($(this).html());
        $(this).parent().parent().parent().parent().children(".text").attr('name', district); //ex	
        $(this).parent().parent().parent().css("display", "none");
        if (district == "0") {
            $(".search-box .district-container .text").html("--- Quận/Huyện ---");
        }
    });

    $(".search-box .view .exit").click(function() {
        $(this).parent().css("display", "none");
    });

    $(".search-box .price-container .view li").click(function() {
        var price = $(this).attr('name');
        if (price == "0") {
            $(".search-box .price-container .text").html("-------- Giá ------");
        }
    });
    $(".search-box .direct-container .view li").click(function() {
        var direct = $(this).attr('name');
        if (direct == "0") {
            $(".search-box .direct-container .text").html("------ Hướng -----");
        }
    });

    $(".search-box .square-container .view li").click(function() {
        var square = $(this).attr('name');
        if (square == "0") {
            $(".search-box .square-container .text").html("------ Diện tích -----");
        }
    });

    //province click -> load district
    $(".search-box .province-container .view li").click(function() {
        $('.hddWard').val("0");
        $('.hddStreet').val("0");
        $('.search-box .location').val("");
        LoadDistricts();
    });

    $(".search-box .button-search").click(function() {
        var loaitin = locdau($('.search-box .demand-container .text').html());
        var loaibds = locdau($('.search-box .property-type-container .text').html());
        var tentinh = locdau($('.search-box .province-container .text').html());
        var matinh = $('.search-box .province-container .text').attr('name');
        var tenhuyen = locdau($('.search-box .district-container .text').html());
        var mahuyen = $('.search-box .district-container .text').attr('name');
        var phuong = $('.hddWard').val();
        var tenphuong = "";
        var maphuong = "";
        var duong = $('.hddStreet').val();
        var tenduong = "";
        var maduong = "";
        var gia = $('.search-box .price-container .text').attr('name');
        var dt = $('.search-box .square-container .text').attr('name');
        var huong = $('.search-box .direct-container .text').attr('name');
        var url = "";
        var params = "";
        if ($('.search-box .property-type-container .text').attr("name") == 0)//chọn tất cả
            loaibds = "nha-dat";

        if (phuong != "0") {
            maphuong = phuong.split(":")[0];
            tenphuong = phuong.split(":")[1];
        }
        if (duong != "0") {
            maduong = duong.split(":")[0];
            tenduong = duong.split(":")[1];
        }
        if (dt == 0 && gia == 0 && huong == 0)
            params = "";
        else
            params = "?dt=" + dt + "&gia=" + gia + "&huong=" + huong;

        if (loaibds == "nha") {
            url = "/" + loaitin + "-nha";
            if (maphuong != 0 || maduong != 0) {//có chọn đường hoặc phường
                if (maduong != 0) {//chọn đường
                    url += "-" + tenduong + "-" + tenhuyen + "-d" + maduong;
                } else {
                    url += "-" + tenphuong + "-" + tenhuyen + "-p" + maphuong;
                }
            } else {
                if (matinh != 0) {//có chọn tỉnh
                    if (mahuyen != 0) {//chọn huyện 
                        url += "-" + tenhuyen + "-" + tentinh + "-q" + mahuyen;
                    } else {
                        url += "-" + tentinh + "-t" + matinh;
                    }
                }
            }
            url = url + ".htm" + params;
        } else {
            url = "/nha-dat/" + loaitin + "/" + loaibds;
            if (maphuong != 0 || maduong != 0) {//có chọn đường hoặc phường
                if (maduong != 0) {//chọn đường
                    url += "/" + tenduong + "-" + tenhuyen + "-dp" + maduong;
                } else {
                    url += "/" + tenphuong + "-" + tenhuyen + "-px" + maphuong;
                }
            } else {
                if (matinh != 0) {//có chọn tỉnh
                    if (mahuyen != 0) {//chọn huyện 
                        url += "/" + tentinh + "/" + mahuyen + "/" + tenhuyen;
                    } else {
                        url += "/" + matinh + "/" + tentinh;
                    }
                }
            }
            url = url + ".html" + params;
        }
        window.parent.location = url;
    });

    $('#right .list-agent .expand').click(function() {
        $(this).parents('.item').css('height', 'auto');
        $(this).hide();
    });

    $('.newest-property-box .item').click(function() {
        var url = $(this).attr("link");
        location.href = url;
    });

    $("#right .rating-box input[type='button']").click(function() {
        var valuevote = $("#right .rating-box input[type='radio']:checked").val();
        if (valuevote == null) {
            alert("Bạn chưa điểm đánh giá.");
            return;
        }
        var numberOfVote = $('#right .rating-box label');
        var number = parseInt(numberOfVote.html().replace(/\./g, "")) + 1;
        numberOfVote.html(number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));


        alert("Đã cập nhật đánh giá của bạn");
    });

    $(".right-static-navigation>div").click(function() {
        var id = $(this).attr('name');
        var url = $(".navigation-hidden-link span[name=" + id + "]").html();
        location.href = url;
    });

    $('.property .detail a').click(function() {
        if ($(this).attr('href').indexOf('alonhadat.com.vn') == -1)
            return false; // ko cho chuyển trang khi click vào link trong nội dung
    });
    $("body").on('click', '.property .image-tab .view-map', function(event) {
        $(this).addClass('selected');
        $('.property .image-tab .view-image ').removeClass('selected');
        $('.property .images').hide();
        
        if ($(".property #map").length != 0)
            $('.property #map').show();
        else
            //ViewPropertyInMap($(this).attr('lat'), $(this).attr('lng'));
            ViewPropertyInFrame($(this).attr('lat'), $(this).attr('lng'));
    });
    $("body").on('click', '.property .image-tab .view-image', function(event) {
        $(this).addClass('selected');
        $('.property .image-tab .view-map ').removeClass('selected');
        $('.property #map').hide();
        $('.property .images').show();
    });

    $("body").on('click', '.property .utility .save', function(event) {
        SaveProperty();
        $(this).html("Đã lưu");
    });

    $("body").on('click', '.reflect-form .content .reason span', function() {
        var reason = $(this).closest('div').find('input');
        if (reason.is(':checked')) {
            reason.removeAttr('checked');
        } else {
            reason.attr('checked', 'checked');
        }
    });

    $("body").on('click', '.reflect-form .chkdayignore', function() {
        if ($('.chkdayignore').is(':checked')) {
            $('.dayignore').removeAttr("disabled");
            $('.dayignore').css('background', '#ffffff');
        }
        else {
            $('.dayignore').attr("disabled", "disabled");
            $('.dayignore').css('background', '#eeeeee');
        }
    });

    $("body").on('click', '.reflect-form .bdayignore', function() {
        var checkbox = $(this).closest('div').find('.chkdayignore');
        if (checkbox.is(':checked')) {
            checkbox.removeAttr('checked');
            $('.dayignore').attr("disabled", "disabled");
            $('.dayignore').css('background', '#eeeeee');
        }
        else {
            checkbox.attr("checked", 'checked');
            $('.dayignore').removeAttr("disabled");
            $('.dayignore').css('background', '#ffffff');
        }
    });

    $("body").on('click', '.reflect-form .btnSubmit', function() {
        ReflectSend();
    });
});

$(document).ready(function() {
    //SEARCH ==============
    InitSearchBox();    
    var COMMAND_LOCATION_AUTO_COMPLETE = 22;
    $('.search-box .location').focus(function() {
        if ($('.search-box .location').val().length < 10) {
            $('.search-box .suggesstion').show();
        }
        setInterval(function() {
            if ($('.search-box .location').val().length > 10) {
                $('.search-box .suggesstion').hide();
            }
        }, 5000); //5s tự động ẩn
    });

    $('.search-box .location').blur(function() {
        $('.search-box .suggesstion').hide();
    });

    $('.search-box .location').autocomplete({
        serviceUrl: '/handler/Handler.ashx?command=' + COMMAND_LOCATION_AUTO_COMPLETE,
        minChars: 3,
        autoSelectFirst: true,
        formatResult: function(suggestion, currentValue) {
            var keywords = currentValue.split(" ");
            var words = suggestion.value.split(" ");
            var i = 0;
            var j = 0;
            var result = "";
            for (; i < keywords.length; ++i) {
                for (; j < words.length; ++j) {
                    if (locdau(keywords[i]) == locdau(words[j])) {
                        words[j] = "<strong>" + words[j] + "</strong>";
                        break;
                    }
                }
            }
            for (i = 0; i < words.length; ++i) {
                result += " " + words[i];
            }
            return result;
        },
        onSelect: function(suggestion) {
            $('.search-box .suggesstion').hide(); //ẩn suggestion
            var address = suggestion.data.split(';');
            if (address.length == 5) {//có dự án
                window.parent.location = address[4];
                return;
            }

            var provinceName = $(".search-box .province-container .view li[name='" + address[0] + "']").html();
            $(".search-box .province-container .text").html(provinceName);
            $(".search-box .province-container .text").attr('name', address[0]);
            $('.hddWard').val(address[2]);
            $('.hddStreet').val(address[3]);
            LoadDistricts(address[1]);
        }
    });
});

function LoadDistricts(districtid) {
    $(".search-box .district-container .text").html("--- Quận/Huyện ---");
    $(".search-box .district-container .text").attr('name', '0');
    $('.district-container .district').children().remove();
    var maTinh = $(".search-box .province-container .text").attr('name');
    var url = "/handler/Handler.ashx?command=" + COMMAND_GET_DISTRICTS_UL_FORMAT + "&maTinh=" + maTinh;
    $.post(url, function(data) {
        $('.district-container .district').html(data);
        if (districtid != null) {
            var districtName = $(".search-box .district-container .view li[name='" + districtid + "']").html();
            if (districtid == "0") {
                $(".search-box .district-container .text").html("--- Quận/Huyện ---");
            } else {
                $(".search-box .district-container .text").html(districtName);
            }
            $(".search-box .district-container .text").attr('name', districtid);
        }
        $('.ajax-loading').hide();
    });
}

function InitSearchBox() {
    var demandId = $('.hddSDemandId').val();
    var propertyTypeId = $('.hddSPropertyTypeId').val();
    var provinceId = $('.hddSProvinceId').val();
    var districtId = $('.hddSDistrictId').val();
    var squareId = $('.hddSSquareId').val();
    var directId = $('.hddSDirectId').val();
    var priceId = $('.hddSPriceId').val();
    var searchText = $('.hddSearchText').val();

    //search text
    if (searchText != null) {
        $('.location').val(searchText);
    }
    
    //demand
    var demandName = $(".search-box .demand-container .view li[name='" + demandId + "']").html();
    $(".search-box .demand-container .text").html(demandName);
    $(".search-box .demand-container .text").attr('name', demandId);

    //property type
    var propertyTypeName = $(".search-box .property-type-container .view li[name='" + propertyTypeId + "']").html();

    if (propertyTypeId == "0") {
        $(".search-box .property-type-container .text").html("---- Loại BĐS ----");
    } else {
        $(".search-box .property-type-container .text").html(propertyTypeName);
    }
    $(".search-box .property-type-container .text").attr('name', propertyTypeId);

    //province
    var provinceName = $(".search-box .province-container .view li[name='" + provinceId + "']").html();
    $(".search-box .province-container .text").attr('name', provinceId);
    if (provinceId == "0") {
        $(".search-box .province-container .text").html("---- Tỉnh/Thành ----");
    } else {
    $(".search-box .province-container .text").html(provinceName);
    LoadDistricts(districtId);
    }    

    //Square
    var squareName = $(".search-box .square-container .view li[name='" + squareId + "']").html();
    if (squareId == "0")
        $(".search-box .square-container .text").html("------ Diện tích -----");
    else
        $(".search-box .square-container .text").html(squareName);
    $(".search-box .square-container .text").attr('name', squareId);

    //direct
    var directName = $(".search-box .direct-container .view li[name='" + directId + "']").html();
    if (directId == "0") {
        $(".search-box .direct-container .text").html("------ Hướng -----");
    } else {
        $(".search-box .direct-container .text").html(directName);
    }
    $(".search-box .direct-container .text").attr('name', directId);

    
    //price
    var priceName = $(".search-box .price-container .view li[name='" + priceId + "']").html();
    if (priceId == "0") {
        $(".search-box .price-container .text").html("-------- Giá ------");
    } else {
        $(".search-box .price-container .text").html(priceName);
    }
    $(".search-box .price-container .text").attr('name', priceId);
}

function InitPropertyListPageNavigation() {
    var demandId = $('.hddSDemandId').val();
    var propertyTypeId = $('.hddSPropertyTypeId').val();
    var provinceId = $('.hddSProvinceId').val();
    var districtId = $('.hddSDistrictId').val();
    var wardId = $('.hddSWardId').val();
    var streetId = $('.hddSStreetId').val();
    
    //bỏ trang toàn quốc
    if (provinceId == 0)
        return;

    var url = "/handler/Handler.ashx?command=" + COMMAND_GET_PROPERTY_LIST_NAVIGATION + "&version=1";
    $.post(url, {
        demand: demandId,
        propertytype: propertyTypeId,
        province: provinceId,
        district: districtId,
        ward: wardId,
        street: streetId
    }, function(data) {
        $('.right-navigation-box .temp-navigation').remove();
        if (data != "")
            $('.right-navigation-box').append(data);
    });
}

//chi tiet tin
function showImage(s) {
    document.getElementById('limage').src = s;
}

//cảnh báo
function InitRentNotice() {
    var loaitin = $('.property #hddPropertyType').val();
    if (loaitin == 2) {
        $('.property .contact').after(""
        + "<ul class='rent-notice'>"
        + "<div>Lưu ý khi thuê nhà, phòng trọ:</div>"
        + "<li>Xác minh người cho thuê, tránh bị kẻ gian lừa đảo</li>"
        + "<li><b style='color:red'>Tránh</b> trả phí dịch vụ tìm nhà nếu không có hợp đồng</li>"
        + "<li>Giá thuê, tiền cọc, phí internet, phí giữ xe, ...</li>"
        + "<li><a href='/tin-tuc-c4/kinh-nghiem-thue-nhung-luu-y-khi-tim-thue-nha-phong-tro-id483.html' target='_blank' >Xem thêm kinh nghiệm thuê nhà, phòng trọ</a></li>"
        + "<div class='clear'></div>"
        + "</ul>");
    }
}