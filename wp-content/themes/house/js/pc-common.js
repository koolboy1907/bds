﻿var COMMAND_LOGIN = 6;
var COMMAND_LOGOUT = 7;
var COMMAND_RECOVER = 8;
var COMMAND_REGISTER = 9;
var COMMAND_CHECK_LOGIN = 10;
var COMMAND_GET_BANNERS = 100;
jQuery.fn.center = function() {
    this.css("top", ($(window).height() - $('.popup-layout').height()) / 2 + $(window).scrollTop() - 300 + "px");
    return this;
}
jQuery.fn.center = function(container) {
    var top = ($(window).height() - $(container).height()) / 2 + $(window).scrollTop();
    if (top < 50)
        top = 50; ;
    this.css("top", top + "px");
    this.css("left", ($(window).width() - $(container).width()) / 2 + "px");

    return this;
}
String.prototype.replaceAll = function(find, replace) {
    var str = this;
    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
};

$(document).ready(function() {
    $('.login-form').keypress(function(e) {

        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            Login();
        }
    });
    $('.recover-form').keypress(function(e) {

        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            Recover();
        }
    });
    //============================= REGISTER FORM ==============================//
    $("body").on('click', '.register-form .agent', function() {
        var agent = $('.register-form .agent:checked').val();
        if (agent == 2)
            $('.register-form .expand').show();
        else
            $('.register-form .expand').hide();
    });

    $("body").on('click', '.register-form .button .close', function(e) {
        $('.register-form').hide();
        $('#fade').hide();
    });
    $('.register-form').keypress(function(e) {

        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            if ($('.register-form .message-box').is(":hidden")) {
                RegisterMember();
            }
        }
    });
    $("body").on('focus', '.register-form .row input', function(e) {
        $(this).parents('.row').find('.suggesstion').show();
    });
    $("body").on('blur', '.register-form .row input', function(e) {
        $(this).parents('.row').find('.suggesstion').hide();
    });
    $("body").on('focus', '.register-form .row textarea', function(e) {
        $(this).parents('.row').find('.suggesstion').show();
    });
    $("body").on('blur', '.register-form .row textarea', function(e) {
        $(this).parents('.row').find('.suggesstion').hide();
    });

    $("body").on('click', '.register-form .message-box .button .success', function(e) {
        $(this).css('display', 'none'); //button success
        $(this).parent().parent().css('display', 'none'); //message-box
        $('.register-form .loading').hide(); //loading
        $(this).parent().parent().parent().css('display', 'none'); //register form	
        $('#fade').css('display', 'none');

        var currentURL = window.location.href;

        if (currentURL.indexOf("/dang-tin") == -1) {
            location.href = "/quan-ly-ca-nhan.html";
        } else {
            var contact = $('.register-form #username').val();
            var fone1 = $('.register-form #phone1').val();
            var fone2 = $('.register-form #phone2').val();
            if (fone2 != "")
                fone1 = fone1 + " / " + fone2;
            $('.post-content .lienhe').val(contact);
            $('.post-content .dienthoai').val(fone1);
        }
    });

    $("body").on('click', '.register-form .message-box .button .fail', function(e) {
        $(this).css('display', 'none'); //button fail 
        $('.register-form .loading').hide(); //loading
        $(this).parent().parent().css('display', 'none'); //message-box	    
        //reset form
    });

    //CLOSE BANNER
    $("body").on('click', '.banner-close', function(e) {
        var bannerId = $(this).attr('value');

        var lstbannerId = Cookies.get('banner_pc');
        if (lstbannerId == "" || lstbannerId == null) {
            lstbannerId = bannerId;
        } else {
            lstbannerId += "," + bannerId;
        }
        Cookies.set('banner_pc', lstbannerId);
        $(this).closest('.item').hide();
    });
});
function closeForm(formname) {
    document.getElementById(formname).style.display = 'none';
    document.getElementById('fade').style.display = 'none';
}

function Login() {
    var loginname = $('.login-form #account').val();
    var password = $('.login-form #password').val();
    var remember = "";
    if (loginname.length == 0 || password.length == 0) {
        alert("Chưa nhập tên truy cập hoặc mật khẩu");
        return;
    }
    if ($('.login-form #remember').is(':checked'))
        remember = "on";
    else
        remember = "";
        
    $('.login-form .loading').show();
    var url = "/handler/Handler.ashx?command=" + COMMAND_LOGIN;
    $.post(url, {
        loginname: loginname,
        password: password,
        remember: remember
    }).success(function(data) {
        var fields = data.split(/;/);
        var type = fields[0];
        var message = fields[1];

        if (type == "1") {
            closeForm('login-form');
            $('.reg-menu-box .reg-menu').html('<span class="view-mobile" onclick="ViewWebVersion(2)">Mobile</span><a class="regular-post" href="/dang-tin-nha-dat.html">Đăng tin nhà đất</a><a class="advance-post" href="/dang-tin-nang-cao.html" rel="nofollow">Đăng tin dự án</a><a href="/quan-ly-ca-nhan.html" rel="nofollow"><span class="user">Quản lý cá nhân</span></a> &nbsp; (<a href="/handler/Handler.ashx?command=7"><span class="exit">Thoát</span></a>)');

            //hide login
            $('.reg-menu-box .register').hide();
            $('.reg-menu-box .login').hide();
            var currentURL = window.location.href;

            if (currentURL.indexOf("/dang-tin") == -1) {
                location.href = "/quan-ly-ca-nhan.html";
            } else {
                //đang ở trang đăng tin
                $('.post-content .lienhe').val(fields[1]);
                $('.post-content .dienthoai').val(fields[2]);
            }

        } else {
            if (type == 2) {
                $('.login-form .input-form .message').html(message);
                $('.login-form .input-form .message').append("</br>Vui lòng tham khảo <a href='/huong-dan/quy-dinh-su-dung' style='color:blue'>Quy định đăng tin</a>");
            } else {
                //fail
                $('.login-form .input-form .message').html(message);
            }
        }
        $('.login-form .loading').hide();
    }).error(function(data) {
        alert("Xảy ra lỗi!");
    });
}

function RegisterMember() {
    var account = $('.register-form #account').val();
    var password1 = $('.register-form #password1').val();
    var password2 = $('.register-form #password2').val();
    var username = $('.register-form #username').val();
    var phone1 = $('.register-form #phone1').val();
    var phone2 = $('.register-form #phone2').val();
    var email1 = $('.register-form #email1').val();
    var email2 = $('.register-form #email2').val();
    var province = $('.register-form #register_province').val();
    var district = $('.register-form #register_district').val();
    var agent = $('.register-form .agent:checked').val();
    var loaibds = $('.register-form #loaibds').val();
    var province1 = $('.register-form #register_province1').val();
    var district1 = $('.register-form #register_district1').val();
    var introduce = $('.register-form #introduce').val();
    var captcha = $('.register-form #captcha').val();
    var noemail = "";
    if ($('.register-form .no-email').is(':checked')) {
        noemail = "on";
    }
    $('.register-form *').removeClass('fielderror');
    //account
    var re = new RegExp("^[a-zA-Z0-9_-]{3,30}$");
    var m = re.exec(account);
    if (m == null) {
        $('.register-form #account').addClass('fielderror');
        alert("Tên truy cập không hợp lệ");
        return;
    }
    if (account.length < 3 || account.length > 30) {
        $('.register-form #account').addClass('fielderror');
        alert("Tên truy cập phải lớn hơn 3 và nhỏ hơn 30 ký tự");
        return;
    }
    if (password1.length < 6 || password1.length > 30) {
        $('.register-form #password1').addClass('fielderror');
        alert("Mật khẩu phải lớn hơn 6 và nhỏ hơn 30 ký tự");
        return;
    }

    if (password1 != password2) {
        $('.register-form #password1').addClass('fielderror');
        $('.register-form #password2').addClass('fielderror');
        alert("Mật khẩu được nhập phải giống nhau");
        return;
    }

    if (username.length < 3 || username.length > 45) {
        $('.register-form #username').addClass('fielderror');
        alert("Họ tên từ 3 đến 45 ký tự");
        return;
    }
    //check phone 1
    re = new RegExp("^[0-9]{8,12}$");
    m = re.exec(phone1);
    if (m == null) {
        $('.register-form #phone1').addClass('fielderror');
        alert("Số điện thoại không hợp lệ");
        return;
    }
    if (phone2 != "") {
        //check phone 2
        re = new RegExp("^[0-9]{8,12}$");
        m = re.exec(phone2);
        if (m == null) {
            $('.register-form #phone2').addClass('fielderror');
            alert("Số điện thoại không hợp lệ");
            return;
        }
        //
        if (phone1 == phone2) {
            $('.register-form #phone1').addClass('fielderror');
            $('.register-form #phone2').addClass('fielderror');
            alert("Số điện thoại phải khác nhau");
            return;
        }
    }
    if (noemail == "") {
        re = new RegExp("^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$");
        m = re.exec(email1);

        if (m == null) {
            $('.register-form #email1').addClass('fielderror');
            alert("Email không hợp lệ");
            return;
        }

        if (email1 != email2) {
            $('.register-form #email1').addClass('fielderror');
            $('.register-form #email2').addClass('fielderror');
            alert("Email được nhập phải giống nhau");
            return;
        }
    }
    if (province == 0) {
        $('.register-form #register_province').addClass('fielderror');
        alert("Chưa chọn Tỉnh/Thành");
        return;
    }
    if (district == 0) {
        $('.register-form #register_district').addClass('fielderror');
        alert("Chưa chọn Quận/Huyện");
        return;
    }
    if (agent == null) {
        $('.register-form .agent').addClass('fielderror');
        alert("Chưa chọn loại tài khoản");
        return;
    } else 
    {
        if (agent == 2) {
            if(loaibds == '0;0'){
            $('.register-form #loaibds').addClass('fielderror');
                alert("Chưa chọn loại BĐS môi giới chính");
                return;
            }
            if (province1 == 0) {
                $('.register-form #register_province1').addClass('fielderror');
                alert("Chưa chọn khu vực môi giới chính");
                return;
            }
            if (district1 == 0) {
                $('.register-form #register_district1').addClass('fielderror');
                alert("Chưa chọn khu vực môi giới chính");
                return;
            }
            if (introduce.length <= 100) {
                $('.register-form #introduce').addClass('fielderror');
                alert("Giới thiệu về hoạt động môi giới phải lớn hơn 100 ký tự");
                return;
            }
        }
    }
    if (captcha.length == 0) {
        $('.register-form #captcha').addClass('fielderror');
        alert("Chưa nhập mã an toàn");
        return;
    }
    $('.register-form .loading').show();

    var url = "/handler/Handler.ashx?command=" + COMMAND_REGISTER;
    $.post(url,
	    {
	        account: account,
	        password: password1,
	        username: username,
	        phone1: phone1,
	        phone2: phone2,
	        email: email1,
	        noemail: noemail,
	        province: province,
	        district: district,
	        agent: agent,
	        loaibds: loaibds,
	        province1: province1,
	        district1: district1,
	        introduce: introduce,
	        captcha: captcha
	    }).success(function(data) {
	        var fields = data.split(/;/);
	        type = fields[0];
	        message = fields[1];

	        //fail
	        if (type == "0") {
	            $('.register-form .message-box .message').html(message);
	            $('.register-form .message-box').css('display', 'block');
	            //show button fail
	            $('.register-form .message-box .fail').css('display', 'inline-block');
	            $('.register-form .message-box .success').css('display', 'none');
	            $('.register-form img.captchagenerator').attr("src", "/CaptchaGenerator.ashx?t=" + Math.floor((Math.random() * 100000) + 1)); //reload captcha          
	        } else {

	            $('.register-form .message-box .message').html(message);
	            $('.register-form .message-box').css('display', 'block');
	            //show button success
	            $('.register-form .message-box .success').css('display', 'inline-block');
	            $('.register-form .message-box .fail').css('display', 'none');
	            $('.reg-menu-box .reg-menu').html('<a class="regular-post" href="/dang-tin-nha-dat.html">Đăng tin nhà đất</a><a class="advance-post" href="/dang-tin-nang-cao.html" rel="nofollow">Đăng tin dự án</a><a href="/quan-ly-ca-nhan.html" rel="nofollow"><span class="user">Quản lý cá nhân</span></a> &nbsp; (<a href="/handler/Handler.ashx?command=7"><span class="exit">Thoát</span></a>)');
	        }
	        GoTop(0);
	    }).error(function(data) {
	        alert("Xảy ra lỗi, vui lòng thử lại sau");
	        $('.register-form img.captchagenerator').attr("src", "/CaptchaGenerator.ashx?t=" + Math.floor((Math.random() * 100000) + 1)); //reload captcha   
	        $('.register-form .loading').hide();
	    });
}

function Recover() {
    var loginname = $('.recover-form #username').val();
    var email = $('.recover-form #email').val();
    if (loginname.length == 0) {
        alert("Chưa nhập tên truy cập.");
        return;
    }
    if (email.length == 0) {
        alert("Chưa nhập email.");
        return;
    }

    $('.recover-form .loading').show();
    var url = "/handler/Handler.ashx?command=" + COMMAND_RECOVER;
    $.post(url, {
        loginname: loginname,
        email: email
    }).success(function(data) {
        var fields = data.split(/;/);
        var type = fields[0];
        var message = fields[1];
        if (type == "1") {
            alert(message);
            closeForm('recover-form');
        } else {
            $('.recover-form .input-form .message').html('<span style=\'color:red\'>' + message + '</span>');
        }
        $('.recover-form .loading').hide();
    }).error(function() {
        alert("Xảy ra lỗi");
        $('.recover-form .loading').hide();
    });
}

$(document).ready(function() {
    var pageId = 0;
    var provinceId = 0;
    if ($('.page-id').val() == null) {
        pageId = 0;
    } else {
        pageId = $('.page-id').val();
    }
    if ($('.province-id').val() == null)
        provinceId = 0;
    else
        provinceId = $('.province-id').val();

    var position = { 1: "top", 2: "bottom", 3: "left", 4: "right", 5: "center" };
    var url = "/handler/Handler.ashx?command=" + COMMAND_GET_BANNERS + "&pageId=" + pageId + "&provinceId=" + provinceId + "&version=1";
    $.post(url, function(data) {
        var items = jQuery.parseJSON(data);
        var data_length = items.length;
        for (var i = 0; i < data_length; i++) {
            var id = items[i]["Id"];
            var file = items[i]["FileUrl"];
            var width = items[i]["Width"];
            var height = items[i]["Height"];
            var type = items[i]["Type"]
            var pos = items[i]["Position"];
            var IsExistFile = items[i]["IsExistFile"];
            var WebsiteUrl = items[i]["WebsiteUrl"];
            if (IsExistFile == 0) {
                $('.banner-' + position[pos]).append('<div class="item"><span class="banner-close" value="'+id+'" title="Ẩn quảng cáo này"></span><div>File not found!</div><div class="over"></div></div>');
                continue;
            }
            if (WebsiteUrl == '#') {
                if (type == 1) {
                    $('.banner-' + position[pos]).append('<div class="item"><span class="banner-close" value="' + id + '" title="Ẩn quảng cáo này"></span><img src="' + file + '" width="' + width + '" height="' + height + '"></img><div class="over"></div></div>');
                } else {
                    if (type == 2) { //flash
                        $('.banner-' + position[pos]).append('<div class="item"><span class="banner-close" value="' + id + '" title="Ẩn quảng cáo này"></span><object><param name="wmode" value="transparent"><embed src="' + file + '" allowscriptaccess="always" wmode="transparent" quality="high" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=Shockwaveflash" type="application/x-shockwave-flash" width="' + width + '" height="' + height + '"></object><div class="over"></div></div>');
                    } else {
                    $('.banner-' + position[pos]).append('<div class="item"><span class="banner-close" value="' + id + '" title="Ẩn quảng cáo này"></span><iframe src="' + file + '" width="' + width + '" height="' + height + '" frameborder="0" scrolling="no" style="overflow:hidden"></iframe><div class="over"></div></div>');
                    }
                }
            } else {
                if (type == 1) {
                    $('.banner-' + position[pos]).append('<div class="item"><span class="banner-close" value="' + id + '" title="Ẩn quảng cáo này"></span><img src="' + file + '" width="' + width + '" height="' + height + '"></img><div class="over" onclick="ClickBanner(' + id + ')"></div></div>');
                } else {
                    if (type == 2) { //flash
                        $('.banner-' + position[pos]).append('<div class="item"><span class="banner-close" value="' + id + '" title="Ẩn quảng cáo này"></span><object><param name="wmode" value="transparent"><embed src="' + file + '" allowscriptaccess="always" wmode="transparent" quality="high" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=Shockwaveflash" type="application/x-shockwave-flash" width="' + width + '" height="' + height + '"></object><div class="over" onclick="ClickBanner(' + id + ')"></div></div>');
                    } else {
                    $('.banner-' + position[pos]).append('<div class="item"><span class="banner-close" value="' + id + '" title="Ẩn quảng cáo này"></span><iframe src="' + file + '" width="' + width + '" height="' + height + '" frameborder="0" scrolling="no" style="overflow:hidden"></iframe><div class="over" onclick="ClickBanner(' + id + ')"></div></div>');
                    }
                }
            }
        }
        ConfigBannerLEFTRIGHT();
    });

    $('.top-contact').html(""
     + "<div class='row1'>&nbsp;<img src='/publish/img/new.gif'>&nbsp;Hỗ trợ đăng tin: "
        + "<span class='staff'><span class='name'>Ms.Trang</span> - <span class='number'>0937.912.433</span></span> | <span class='staff'><span class='name'>Ms.Nguyên</span> - <span class='number'>0945.243.197</span></span> | <span class='staff'><span class='name'>Ms.Liên</span> - <span class='number'>0967.827.722</span></span> | <span class='staff'><span class='name'>Ms.Huyền</span> - <span class='number'>0906.438.713</span></span> | <span class='staff'><span class='name'>Ms.Giang</span> - <span class='number'>0976.804.401</span></span>"
        + "<span class='help'><a href='/huong-dan/huong-dan'>Trợ giúp - hướng dẫn</a></span></div>"
        + "<div class='row2'>Thời gian làm việc: <b class='work-time'>7h30 - 12h; 14h - 17h30</b> (từ thứ Hai - thứ Bảy)  | Email: contact@alonhadat.com.vn</div> "
     + "</div>");
});
function ClickBanner(id) {
    var url = "/bannerclick.aspx?id=" + id;
    window.open(url, '_blank');
}

function ConfigBannerLEFTRIGHT() {
    if ($('.banner-left').length == 0 && $('.banner-right').length == 0)
        return;
    $('body').css('min-width', '1280px');//cho hiển thị trên mobile/tablet
    var bodywidth = 1000;
    var widthleft = 130;
    var widthright = 130;

    var xright = (($(document).width() - bodywidth) / 2) + bodywidth + 10;
    var xleft = (($(document).width() - bodywidth) / 2) - widthleft - 10;


    if (navigator.userAgent.indexOf("Firefox") != -1) {
        $('.banner-right').css('top', 0);
        $('.banner-left').css('top', 0);
    }
    else {
        RePosition();
    }

    $(window).scroll(function() {
        RePosition();
    });

    $(window).resize(function() {
        RePosition();
    });

    function RePosition() {
        xright = (($(document.body).width() - bodywidth) / 2) + bodywidth + 10;
        xleft = (($(document.body).width() - bodywidth) / 2) - widthright - 10;
        var $toado_old = 0;
        var $toado_curr = $(window).scrollTop() + $toado_old;
        $('.banner-right').css('height', $(window).height());
        $('.banner-left').css('height', $(window).height());
        $('.banner-left').stop().animate({ 'top': $toado_curr - $toado_old, 'left': xleft }, 400)//Cách TOP 0px
        $('.banner-right').stop().animate({ 'top': $toado_curr - $toado_old, 'right': xleft }, 400)//Cách TOP 0px
    }
    //RePosition();
}
$(document).ready(function() {
    var url = "/handler/Handler.ashx?command=19";
    $.post(url).success(function(data) {
        $('.poll-box .content').html(data);
    }).error(function(data) {

    });
});


function SubmitVote() {
    var valuevote = $(".poll-box input[name='vote']:checked").val();
    if (valuevote == null) {
        alert("Bạn chưa chọn câu trả lời.");
        return;
    }
    var row = $(".poll-box input[name='vote']:checked").parent('div');

    var url = "/handler/Handler.ashx?command=18&value=" + valuevote;
    $.post(url).success(function(data) {
        var fields = data.split(/;/);
        var type = fields[0];
        var message = fields[1];
        //fail
        if (type == "0") {
            alert("Xảy ra lỗi");
        } if (type == "1") {
            alert("Bạn đã bỏ phiếu rồi.");
        }
        else {
            var numberOfVote = row.find('.numberofvote label');
            var number = parseInt(numberOfVote.html().replace(/\./g, "")) + 1;
            numberOfVote.html(number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            alert("Đã cập nhật phiếu của bạn.");
        }
    }).error(function(data) {

    });
}
function RePostionGuestConfig() {
    if ($('.guest-config .head').html() != "") {
        var xright = (($(document).width() - 1000) / 2);
        $('.guest-config').css('right', xright + "px");
        $('#wrapper').css('padding-bottom', "23px");
    } else {
    $('#wrapper').css('padding-bottom', "0px");
    }
}
