﻿$(document).ready(function() {
    $('#news #slides').slides({
        preload: false,
        preloadImage: '/publish/img/loader.gif',
        play: 10000,
        pause: 5000,
        slideSpeed: 2500,
        hoverPause: true,
        generateNextPrev: false,
        generatePagination: false,
        paginationClass: 'paginationSlide'
    });
    $('.hot-project-box .slides').slides({
        preload: false,
        preloadImage: '/publish/img/loader.gif',
        play: 3000,
        pause: 1,
        slideSpeed: 1000,
        fadeSpeed: 1000,
        hoverPause: true,
        effect: 'fade',
        crossfade: true,
        generateNextPrev: false,
        generatePagination: false,
        paginationClass: 'paginationSlide'
    });
    //SEARCH ==============
    var COMMAND_LOCATION_AUTO_COMPLETE = 22;
    var COMMAND_GET_VIP_PROPERTY = 23;
    $('.search-box .location').focus(function() {
        if ($('.search-box .location').val().length < 10) {
            $('.search-box .suggesstion').show();
        }
        setInterval(function() {
            if ($('.search-box .location').val().length > 10) {
                $('.search-box .suggesstion').hide();
            }
        }, 5000); //5s tự động ẩn
    });

    $('.search-box .location').blur(function() {
        $('.search-box .suggesstion').hide();
    });

    $('.search-box .location').autocomplete({
        serviceUrl: '/handler/Handler.ashx?command=' + COMMAND_LOCATION_AUTO_COMPLETE,
        minChars: 3,
        autoSelectFirst: true,
        formatResult: function(suggestion, currentValue) {
            var keywords = currentValue.split(" ");
            var words = suggestion.value.split(" ");
            var i = 0;
            var j = 0;
            var result = "";
            for (; i < keywords.length; ++i) {
                for (; j < words.length; ++j) {
                    if (locdau(keywords[i]) == locdau(words[j])) {
                        words[j] = "<strong>" + words[j] + "</strong>";
                        break;
                    }
                }
            }
            for (i = 0; i < words.length; ++i) {
                result += " " + words[i];
            }
            return result;
        },
        onSelect: function(suggestion) {
            $('.search-box .suggesstion').hide(); //ẩn suggestion
            var address = suggestion.data.split(';');
            if (address.length == 5) {//có dự án
                window.parent.location = address[4];
                return;
            }
            $('.search-box .province').val(address[0]); //set tinh ID
            $('.hddWard').val(address[2]); //set Id, link Ward : format: Id:link
            $('.hddStreet').val(address[3]); //set Id, link Street
            LoadQuanHuyen(address[1]); //District ID
        }
    });

    //VIP PAGING
    $('.vip-paging span').click(function() {
        var clickedPage = $(this).html();
        var activingPage = $('.vip-paging').find('.active').html();
        if (clickedPage == activingPage)
            return;
        $('.vip-paging span').removeClass('active');
        $(this).addClass('active');
        $('.vip-property-box .loading').show();

        var url = "/handler/Handler.ashx?command=" + COMMAND_GET_VIP_PROPERTY + "&page=" + clickedPage;
        $.post(url, function(data) {
            if (data != "") {
                $('.vip-properties').html(data);
                var offset = $('.vip-property-box').offset();
                $('html,body').animate({ scrollTop: offset.top - 50 }, 0);
            }
            $('.vip-property-box .loading').hide();
        }).error(function(data) {
            $('.vip-property-box .loading').hide();
        });

        //$('.vip-property-box .vip-properties').eq(index).show();
    });
});

function Search() {
    var matinh = $('.search-box .province').val();
    var tentinh = "";
    var mahuyen = $('.search-box .district').val();
    var tenhuyen = "";
    var phuong = $('.hddWard').val();
    var maphuong = "";
    var tenphuong = "";
    var duong = $('.hddStreet').val();
    var tenduong = "";
    var maduong = ""
    var loaitin = $('.search-box .demand').val();
    var loaibds = $('.search-box .property-type').val();
    var huong = $('.search-box .direct').val();
    var dt = $('.search-box .square').val();
    var gia = $('.search-box .price').val();
    var url = "";
    var params = "";
    if (matinh != 0) {
        tentinh = locdau($(".search-box .province option:selected").text());
    }

    if (mahuyen != 0) {
        tenhuyen = locdau($(".search-box .district option:selected").text());
    }

    if (phuong != "0") {
        maphuong = phuong.split(":")[0];
        tenphuong = phuong.split(":")[1];
    }
    if (duong != "0") {
        maduong = duong.split(":")[0];
        tenduong = duong.split(":")[1];
    }
    if (dt == 0 && gia == 0 && huong == 0)
        params = "";
    else
        params = "?dt=" + dt + "&gia=" + gia + "&huong=" + huong;

    if (loaibds == "nha") {
        url = "/" + loaitin + "-nha";
        if (maphuong != 0 || maduong != 0) {//có chọn đường hoặc phường
            if (maduong != 0) {//chọn đường
                url += "-" + tenduong + "-" + tenhuyen + "-d" + maduong;
            } else {
                url += "-" + tenphuong + "-" + tenhuyen + "-p" + maphuong;
            }
        } else {
            if (matinh != 0) {//có chọn tỉnh
                if (mahuyen != 0) {//chọn huyện 
                    url += "-" + tenhuyen + "-" + tentinh + "-q" + mahuyen;
                } else {
                    url += "-" + tentinh + "-t" + matinh;
                }
            }
        }
        url = url + ".htm" + params;
    } else {
        url = "/nha-dat/" + loaitin + "/" + loaibds;
        if (maphuong != 0 || maduong != 0) {//có chọn đường hoặc phường
            if (maduong != 0) {//chọn đường
                url += "/" + tenduong + "-" + tenhuyen + "-dp" + maduong;
            } else {
                url += "/" + tenphuong + "-" + tenhuyen + "-px" + maphuong;
            }
        } else {
            if (matinh != 0) {//có chọn tỉnh
                if (mahuyen != 0) {//chọn huyện 
                    url += "/" + tentinh + "/" + mahuyen + "/" + tenhuyen;
                } else {
                    url += "/" + matinh + "/" + tentinh;
                }
            }
        }
        url = url + ".html" + params;
    }
    window.parent.location = url;
}

function LoadQuanHuyen(value) {
    var province = $('.search-box .province').val();
    var url = "/handler/Handler.ashx?command=2&matinh=" + province;
    $.post(url, function(data) {
        $('.district').html(data);
        if (value != null)
            $('.district').val(value);
    });
}

function huyen_click() {
    var province = $('.search-box .province').val();
    if (province != 0 && $('.search-box .district option').length < 2) {
        LoadQuanHuyen();
    }
}
