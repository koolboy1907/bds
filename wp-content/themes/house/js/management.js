﻿var COMMAND_DELETE_PROPERTY_IMAGE = 1;
var COMMAND_GET_DISTRICTS = 2;
var COMMAND_GET_WARDS = 3;
var COMMAND_CHECK_LOGIN = 10;
var COMMAND_GET_STREETS = 4;
var COMMAND_CHECK_CAPTCHA = 11;
var COMMAND_GET_PROJECTS = 16;

//user
var USER_GET_PROPERTIES = 1;
var USER_RENEW_PROPERTY = 2;
var USER_DELETE_PROPERTY = 3;
var USER_UPDATE_INFO = 4;
var USER_CHANGE_PASSWORD = 5;
var USER_NAP_CARD = 11;
var USER_CHECK_MEMBER_INFORM_POPUP = 12;
var USER_VERIFY_ACCOUNT = 13;
var USER_CHECK_ALLOW_POST = 14;
var USER_RENEW_PACKAGE_LIST = 15;
var USER_RENEW_PACKAGE_BUY = 16;
var USER_RENEW_ALL_PROPERTY = 17;
var USER_SHARE_MONEY = 18;
var USER_GET_BALANCE_OF_MEMBER = 21;
var USER_GET_RENEW_CAPTCHA = 22;
var USER_CHECK_RENEW_CAPTCHA = 23;
var USER_CHECK_PROPERTY_INFORM_POPUP = 29;
var USER_REPOST = 31;
var USER_STOP_PUBLISH_PROPERTY = 32;
var USER_PUBLISH_PROPERTY = 33;
var USER_GROUP_BY_PROPERTY_IN_DISTRICT_BY_MEMBER = 34;

$(document).ready(function() {
    //init personality
    $('.manage-menu li .personality-management li').click(function() {
        GoTop(0);
        $('.manage-menu li ul li').removeClass('selected');
        $(this).addClass('selected');
        var url = $(this).attr('url');
        if (url == "") {
            $('.content-box').html("Chức năng này đang được xây dựng");
            return;
        }
        $('.loading').show();
        $.post(url, function(data) {
            $('.content-box').html(data);
            $('.loading').hide();
        });
    });

    //init utils
    $('.manage-menu li .utils-management li').click(function() {
        GoTop(0);
        $('.manage-menu li ul li').removeClass('selected');
        $(this).addClass('selected');
        var url = $(this).attr('url');
        if (url == "") {
            $('.content-box').html("Chức năng này đang được xây dựng");
            return;
        }
        $('.loading').show();
        $.post(url, function(data) {
            $('.content-box').html(data);
            $('.loading').hide();

            //is transaction history
            if ($('.transactionhistory').length == 1) {
                $(".transactionhistory .tran-date").datepicker();
                $(".transactionhistory .tran-date").datepicker('option', { dateFormat: 'dd/mm/yy' });
                $(".transactionhistory .tran-date").datepicker("setDate", "defaultDate");
            }

        });
    });

    //TRANSACTION HISTORY
    //transaction history paging
    $("body").on('click', '.transactionhistory .paging span', function(event) {
        var page = $(this).attr('page');
        SearchTransactionHistory(page);
    });
    $("body").on('click', '.transactionhistory .tran-clear', function(event) {
        $('.transactionhistory .tran-date').val("");
        SearchTransactionHistory(1);
    });
    //init page
    $('.manage-menu li .message-management li').click(function() {
        $('.manage-menu li ul li').removeClass('selected');
        $(this).addClass('selected');
        var url = $(this).attr('url');
        if (url == "") {
            $('.content-box').html("Chức năng này đang được xây dựng");
            return;
        }
        $('.loading').show();
        $.post(url, function(data) {
            $('.content-box').html(data);
            $('.loading').hide();
        });
    });

    //============= PROPERTY ==============
    //init page
    $('.manage-menu li .property-management li').click(function() {
        $('.manage-menu li ul li').removeClass('selected');
        $(this).addClass('selected');
        var url = $(this).attr('url');
        if (url == "") {
            $('.content-box').html("Chức năng này đang được xây dựng");
            return;
        }
        $('.loading').show();
        $.post(url).success(function(data) {
            LoadListProperty(data);
            $('.loading').hide();
        }).error(function(data) {
            alert("Xảy ra lỗi!");
            $('.loading').hide();
        });
    });
    //property paging
    $("body").on('click', '.list-property .paging span', function(event) {
        var page = $(this).attr('page');
        $("#page").val(page);
        SearchProperty();
    });

    //RENEW
    $("body").on('click', '.list-property .renew', function() {
        var url = "/publish/handler/Handler.ashx?command=" + USER_GET_RENEW_CAPTCHA;
        var id = $(this).parents('tr').find("#id").val();
        var row = $(this).parents('tr');
        $.post(url).success(function(data) {
            var fields = data.split(/;/);
            var type = fields[0];
            data = fields[1];
            if (type == 1) {//show captcha
                $('.list-property .captcha-box').hide(); //ẩn tất cả captcha
                row.find('.captcha-box').html("<div style='font-weight:bold;color:blue;'>Chỉ nhập chữ, số không bị gạch</div><img class='captchagenerator' src='/CaptchaGenerator.ashx?t=" + Math.floor((Math.random() * 100000) + 1) + "' align='center'/><input type='text' class='captcha' maxlength='5'><input type='button' value='Xác thực' class='btncaptcha' />");
                row.find('.captcha-box').show();
                row.find('.captcha-box .captcha').focus();
            } else {
                url = "/publish/handler/Handler.ashx?command=" + USER_RENEW_PROPERTY + "&id=" + id;
                $('.loading').show();
                $.post(url).success(function(data) {
                    var fields = data.split(/;/);
                    var type = fields[0];
                    data = fields[1];
                    //success
                    if (type == "1") {//thành công, trừ số lần UP miễn phí
                        row.find('.updatedate').html(data); //update ngày cập nhật
                        row.find('.freeupdate').html(data); //update ngày làm mới miễn phí
                        row.find('.renew').hide();
                    } else
                        if (type == 2) {//thành công, - số lần lần trong gói UP
                        row.find('.updatedate').html(data);
                        row.find('.renew').hide();

                        var uptimes = parseInt($('.uptimes').html().replace(/\./g, "")) - 1;
                        $('.uptimes').html(uptimes);
                    } else {//xảy ra lỗi
                        alert(data);
                        BackToListProperty();
                    }
                    $('.loading').hide();
                }).error(function(data) {
                    alert("Xảy ra lỗi!");
                    $('.loading').hide();
                });
            }
        });
    });
    //SEND RENEW CAPTCHA
    $("body").on('click', '.list-property .captcha-box .btncaptcha', function() {
        var parents = $(this).parents('.captcha-box');
        var captcha = parents.find('.captcha').val();
        if (captcha == "") {
            alert("Chưa nhập mã an toàn");
            parents.find('.captcha').focus();
            return;
        }
        var url = "/publish/handler/Handler.ashx?command=" + USER_CHECK_RENEW_CAPTCHA + "&captcha=" + captcha;
        $('.loading').show();
        $.post(url).success(function(data) {
            var fields = data.split(/;/);
            var type = fields[0];
            var message = fields[1];
            if (type == 1) {
                parents.hide();
            } else {
                alert(message);
                parents.find('.captchagenerator').attr('src', '/CaptchaGenerator.ashx?t=' + Math.floor((Math.random() * 100000) + 1));
                parents.find('.captcha').focus();
            }
            $('.loading').hide();
        }).error(function(data) {
            alert("Xảy ra lỗi!");
            $('.loading').hide();
        });
    });

    //stop publish  
    $("body").on('click', '.list-property .stoppublish', function() {
        var r = confirm("Bạn có chắc muốn tạm dừng đăng tin này ?");
        if (r == false) {
            return;
        }
        var id = $(this).parents('tr').find("#id").val();
        var row = $(this).parents('tr');
        var url = "/publish/handler/Handler.ashx?command=" + USER_STOP_PUBLISH_PROPERTY + "&id=" + id;
        $('.loading').show();
        $.post(url).success(function(data) {
            var fields = data.split(/;/);
            var type = fields[0];
            var message = fields[1];

            //success
            if (type == "1") {
                row.remove();
            } else {
                alert("Xảy ra lỗi. Vui lòng thử lại.");
            }
            $('.loading').hide();
        }).error(function(data) {
            alert("Xảy ra lỗi!");
            $('.loading').hide();
        });
    });
    //publish  
    $("body").on('click', '.list-property .publishproperty', function() {
        var r = confirm("Bạn có chắc muốn đăng lại tin này ?");
        if (r == false) {
            return;
        }
        var id = $(this).parents('tr').find("#id").val();
        var row = $(this).parents('tr');
        var url = "/publish/handler/Handler.ashx?command=" + USER_PUBLISH_PROPERTY + "&id=" + id;
        $('.loading').show();
        $.post(url).success(function(data) {
            var fields = data.split(/;/);
            var type = fields[0];
            var message = fields[1];

            //success
            if (type == "1") {
                row.remove();
            } else {
                alert("Xảy ra lỗi. Vui lòng thử lại.");
            }
            $('.loading').hide();
        }).error(function(data) {
            alert("Xảy ra lỗi!");
            $('.loading').hide();
        });
    });

    //show repost form
    $("body").on('click', '.list-property .showrepost', function() {
        var id = $(this).parents('tr').find("#id").val();
        var row = $(this).parents('tr');
        var url = "/publish/form/RePostForm.aspx?propertyid=" + id;
        $('.loading').show();
        $.post(url).success(function(data) {

            if ($('.content-box .repost-box').length != 0) {
                $('.content-box .repost-box').html(data);
            } else {
                $('.content-box').append("<div class='repost-box'></div>");
                $('.content-box .repost-box').html(data);
            }
            $('.content-box .list-property-form').hide();
            $('.content-box .repost-box').show();
            $('.loading').hide();
        }).error(function(data) {
            alert("Xảy ra lỗi!");
            $('.loading').hide();
        });
        GoTop(0);
    });

    //delete  
    $("body").on('click', '.list-property .delete', function() {
        var r = confirm("Bạn có chắc muốn xóa tin này ?");
        if (r == false) {
            return;
        }
        var id = $(this).parents('tr').find("#id").val();
        var row = $(this).parents('tr');
        var url = "/publish/handler/Handler.ashx?command=" + USER_DELETE_PROPERTY + "&id=" + id;
        $('.loading').show();
        $.post(url).success(function(data) {
            var fields = data.split(/;/);
            var type = fields[0];
            var message = fields[1];

            //success
            if (type == "1") {
                row.remove();
            } else {
                alert("Xảy ra lỗi. Vui lòng thử lại.");
            }
            $('.loading').hide();
        }).error(function(data) {
            alert("Xảy ra lỗi!");
            $('.loading').hide();
        });
    });

    //delete anh
    $("body").on('click', '.post-content #listthumbnail .remove', function() {

        var r = confirm("Bạn có chắc muốn xóa ảnh này ?")
        if (r == false) {
            return;
        }
        var id = $(this).attr('name');
        var url = "/handler/Handler.ashx?command=" + COMMAND_DELETE_PROPERTY_IMAGE + "&id=" + id;
        var image = $(this).parent('li');
        $.post(url).success(function(data) {
            var fields = data.split(/;/);
            var type = fields[0];
            var message = fields[1];
            if (type == 1)//success
            {
                image.remove();
            } else {
                alert(message);
            }
        }).error(function(data) {
            alert("Xảy ra lỗi!");
        });
    });


    $("body").on('keypress', '.change-password-box', function(e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            ChangePassword();
        }
    });

    $("body").on('keypress', '.user-infor', function(e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            UpdateUserInfor();
        }
    });
});

function SearchTransactionHistory(page) {
    var date = $('.transactionhistory .tran-date').val();
    var url = "/publish/handler/Handler.ashx?command=6&index=" + page + "&date=" + date;
    $('.loading').show();
    $.post(url).success(function(data) {
        $('.content-box').html(data);
        $('.loading').hide();
        GoTop(0);
        $(".transactionhistory .tran-date").datepicker();
        $(".transactionhistory .tran-date").datepicker('option', { dateFormat: 'dd/mm/yy' });
        $(".transactionhistory .tran-date").val(date);//set date
    }).error(function(data) {
        alert("Xảy ra lỗi!");
        $('.loading').hide();
    });
}

function LoadListProperty(data) {
    var searchbox = "<div style='color:red;font-size:17px;'><b>Lưu ý:</b> Thành viên vui lòng xóa các tin đã giao dịch để tránh bị khách hàng phản ánh dẫn đến việc tài khoản bị khóa.</div>"+        
        "<div class='search-box'>"+
            "<span class='property-id'><input type='text' id='searchId' class='searchId' maxlength='90' placeholder='Mã tin'/></span> " +
            "<span class='demand'><select id='demand'>" +
	            "<option value='0'>--- Loại tin ---</option>" +
		        "<option value='1'>Cần bán</option>" +
		        "<option value='2'>Cho thuê</option>" +
		        "<option value='3'>Cần mua</option>" +
		        "<option value='4'>Cần thuê</option>" +
	        "</select></span>" +

	        "<span class='property-type'><select id='propertytype'>" +
		        "<option value='0'>--- Loại BĐS ---</option>" +
		        "<option value='1'>Nhà mặt tiền</option>" +
		        "<option value='2'>Nhà trong hẻm</option>" +
		        "<option value='3'>Biệt thự, nhà liền kề</option>" +
		        "<option value='4'>Căn hộ chung cư</option>" +
		        "<option value='8'>Phòng trọ, nhà trọ</option>" +
		        "<option value='7'>Văn phòng</option>" +
		        "<option value='5'>Kho, xưởng</option>" +
		        "<option value='6'>Nhà hàng, khách sạn</option>" +
		        "<option value='15'>Shop, kiot, quán</option>" +
		        "<option value='9'>Trang trại</option>" +
		        "<option value='10'>Mặt bằng</option>" +
		        "<option value='11'>Đất thổ cư, đất ở</option>" +
		        "<option value='12'>Đất nền, liền kề, đất dự án</option>" +
		        "<option value='13'>Đất nông, lâm nghiệp</option>" +
		        "<option value='14'>Các loại khác</option>" +
	        "</select> </span>" +
	        "<span class='district'><select id='district'>" +
		        "<option value='0'>--- Quận/Huyện ---</option>" +		        
	        "</select></span>" +
	        "<span class='createdate'>Ngày đăng <input type='text' id='createdate' maxlength='15'/></span>" +	        
	        "<span> <input type='button' id='search' style='width:75px; height: 27px;' value='Tìm' onclick='SearchProperty()'/></span>" +
	        "<div class='row-show-box'><div class='buyup' onclick='LoadRenewPackageList()'><img src='/publish/img/buyup.gif'/> Mua lượt UP</div> <div class='rowshow'>Hiển thị<select id='slRowShow' onchange='RowShowChange()'><option value='20'>20</option><option value='50'>50</option><option value='100'>100</option><option value='200'>200</option></select></div></div></div>";
    
            $('.content-box').html("<div class='list-property-form'>" + searchbox + "<div class='property-box'>"+data + "</div></div>");

            $(".search-box #createdate").datepicker();
            $(".search-box #createdate").datepicker('option', { dateFormat: 'yy/mm/dd' });
            GroupByPropertyInDistrictByMember();    
}
function GroupByPropertyInDistrictByMember() {
    var status = $("#status").val();
    var url = "/publish/handler/Handler.ashx?command=" + USER_GROUP_BY_PROPERTY_IN_DISTRICT_BY_MEMBER + "&status=" + status;
    $.post(url).success(function(data) {
        $(".search-box .district select").html(data);
    }).error(function(data) {
        alert("Xảy ra lỗi!");
    });
    //$(".search-box .district select").html("<option value='0'>--- Quận/Huyện ---</option><option value='1'>--- He he ---</option>");
}
function BackToListProperty() {
    var url = "/publish/handler/Handler.ashx?command=1&status=1";
    $('.loading').show();
    $.post(url).success(function(data) {
        LoadListProperty(data);
        $('.loading').hide();
    }).error(function(data) {
        alert("Xảy ra lỗi!");
        $('.loading').hide();
    });
}

function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

function RowShowChange(){
    $("#page").val(1);
    SearchProperty();
}

function SearchProperty() {
    var id = $('.search-box #searchId').val();
    var demand = $('.search-box #demand').val();
    var propertytype = $('.search-box #propertytype').val();
    var district = $('.search-box #district').val();
    var createdate = $('.search-box #createdate').val();
    var status = $("#status").val();
    var page = $("#page").val();
    var rowShow = $("#slRowShow").val();

    var url = "/publish/handler/Handler.ashx?command=" + USER_GET_PROPERTIES +"&status="+status;

    $(".loading").show();
    $.post(url, {
        id: id,
        demand: demand,
        propertytype: propertytype,
        district: district,
        createdate: createdate,
        page: page,
        rowshow: rowShow
    }).success(function(data) {
        $('.property-box').html(data);
        $('.loading').hide();
        GoTop(0);
    }).error(function(data) {
        alert("Xảy ra lỗi!");
        $('.loading').hide();
    });
    GoTop(0);
}

function EditProperty(id, contentType) {
    var url = "";
    if (contentType == 2) {
        url = "/publish/form/PropertyAdvanceAddForm.aspx?id=" + id;
        GetAdvancePostForm(url);
    } else {
        url = "/publish/form/PropertyAddForm.aspx?id=" + id; ;
        GetPostForm(url);
    }
}
function ReloadDistrict() {
    var items = $(".post-content #huyen option").size();
    var tinh = $('.post-content #tinh').val();
    if (tinh != 0 && items < 2) {
        GetDistricts();
    }
}
function GetDistricts() {

    var maTinh = $('.post-content #tinh').val();
    $('.post-content #phuong').html("<option value=''>---- Phường/Xã ----</option>");
    $('.post-content #duong').html("<option value=''>---- Đường/Phố ----</option>");
    $('.post-content #project').html("<option value=''>---- Chọn dự án ----</option>");
    $('.post-content .street-acp-box ul').html("<li isfirst='1' value=''>---- Đường/Phố ----</li>");
    $('.post-content .street-acp-box input').val('---- Đường/Phố ----');
    $('.post-content .project-acp-box ul').html("<li isfirst='1' value=''>---- Chọn dự án----</li>");
    $('.post-content .project-acp-box input').val('---- Chọn dự án ----');
    var url = "/handler/Handler.ashx?command=" + COMMAND_GET_DISTRICTS + "&matinh=" + maTinh;
    $.post(url).success(function(data) {
        $('.post-content #huyen').html(data);
    }).error(function(data) {
        alert("Xảy ra lỗi!");
    });
}
function GetWards() {
    var mahuyen = $('.post-content #huyen').val();
    var url = "/handler/Handler.ashx?command=" + COMMAND_GET_WARDS + "&mahuyen=" + mahuyen;
    $.post(url).success(function(data) {
        $('.post-content #phuong').html(data);
    }).error(function(data) {
        alert("Xảy ra lỗi!");
    });
}
function GetStreets() {
    var mahuyen = $('.post-content #huyen').val();
    var url = "/handler/Handler.ashx?command=" + COMMAND_GET_STREETS + "&mahuyen=" + mahuyen;
    $.post(url).success(function(data) {
        $('.post-content #duong').html(data);
        InitStreetACPValue();
    }).error(function(data) {
        alert("Xảy ra lỗi!");
    });
}
function GetProjects() {
    var mahuyen = $('.post-content #huyen').val();
    var url = "/handler/Handler.ashx?command=" + COMMAND_GET_PROJECTS + "&mahuyen=" + mahuyen;
    $.post(url).success(function(data) {
        $('.post-content #project').html(data);
        InitProjectACPValue();
    }).error(function(data) {
        alert("Xảy ra lỗi!");
    });
}

function getTextPrice(price) {
    var text = ConvertNumberToText(price);
    $('.post-content .price_text').html(text);
}

function ThoaThuan() {
    if ($('.post-content #thoathuan').is(':checked'))
        $('.post-content #gia').attr("disabled", "disabled");
    else
        $('.post-content #gia').removeAttr("disabled");
}

function KXD() {
    if ($('.post-content #kxd').is(':checked'))
        $('.post-content #dientich').attr("disabled", "disabled");
    else
        $('.post-content #dientich').removeAttr("disabled");
}
function SetHeightForIframe(height) {
    $('.content-box iframe').height(height);
}
/*========================= POST FORM ==============*/
$(document).ready(function() {
    $("body").on('blur', '.post-content .diachi', function() {
        UpdateMapByAddress(GetPropertyAddress());
    });
    $("body").on('change', '.post-content .duong', function() {
        UpdateMapByAddress(GetPropertyAddress());
    });

    $("body").on('click', '.post-content .upload-image-box .imgrotate', function() {
        var rotate = $(this).closest('.item').find('.rotate').val();
        if (rotate == "")
            rotate = 0;
        rotate = (parseInt(rotate) + 90) % 360;
        $(this).closest('.item').find('.thumbnail').css('-ms-transform', 'rotate(' + rotate + 'deg)');
        $(this).closest('.item').find('.thumbnail').css('-webkit-transform', 'rotate(' + rotate + 'deg)');
        $(this).closest('.item').find('.thumbnail').css('transform', 'rotate(' + rotate + 'deg)');
        $(this).closest('.item').find('.rotate').val(rotate);
    });

    $("body").on('change', '.post-content .inputimage', function() {
        var noImage = "/publish/img/upload-image.png";
        var defaultImage = "/publish/img/logo.png";
        var filename = $(this).val().split('/').pop().split('\\').pop(); ;
        var input = this;
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(input).parents('.item').find('img').attr('src', e.target.result);
                $(input).parents('.item').find('.btnChooseImage').html("Thay đổi");
                $(input).parents('.item').append('<span class="imgrotate"></span>');
                CaculateFileSize();
            }
            reader.readAsDataURL(input.files[0]);
        }
        $(input).parents('.item').find('.filename').html(filename);
        if (filename == "") {
            $(input).parents('.item').find('img').attr('src', noImage);
            $(input).parents('.item').find('.btnChooseImage').html("Chọn ảnh");
            $(input).parents('.item').find('.imgrotate').remove();
        }
        else
            $(input).parents('.item').find('img').attr('src', defaultImage);
    });
    $("body").on('click', '.post-content .btnRemoveImage', function() {
        var noImage = "/publish/img/upload-image.png";
        input = $(this).parents('.item').find('input');
        input.replaceWith(input.clone(true)); //repace with new
        $(this).parents('.item').find('img').attr('src', noImage);
        $(this).parents('.item').find('.filename').html("");
        $(this).parents('.item').find('.btnChooseImage').html("Chọn ảnh");
        $(this).parents('.item').find('.imgrotate').remove();
        CaculateFileSize();
    });

    $("body").on('click', '.post-content .btnChooseImage', function() {
        $(this).parents('.item').find('input').trigger("click");
    });

    $("body").on('focus', '.post-content .value input', function(e) {
        $('.post-content .suggesstion').hide();
        $(this).parents('.value').find('.suggesstion').show();
    });
    $("body").on('focus', '.post-content .value textarea', function(e) {
        $('.post-content .suggesstion').hide();
    });
    $("body").on('focus', '.post-content .value select', function(e) {
        $('.post-content .suggesstion').hide();
    });
    $("body").on('click', '.post-content .suggesstion', function(e) {
        $('.post-content .suggesstion').hide();
    });
    $("body").on('click', '.post-content .suggesstion .property-exit', function(e) {
        $(this).parents('.suggesstion').remove();
    });
    $("body").on('click', '.post-content .property-over', function(e) {
        $('.post-content .suggesstion').hide();
        $(this).parents('.value').find('.suggesstion').show();
    });
    /*$("body").on('blur', '.post-content .value input', function(e) {
    $(this).parents('.value').find('.suggesstion').hide();
    });*/
    $("body").on('click', '.post-content .update-name', function(e) {
        var tenlienhe = $(this).parents('.value').find('#hddFullName').val();
        $(this).parents('.value').find('#lienhe').val(tenlienhe);
    });
    $("body").on('click', '.post-content .update-phone', function(e) {
        var phone = $(this).parents('.value').find('#hddPhone').val();
        $(this).parents('.value').find('#dienthoai').val(phone);
    });

    $("body").on('click', '.post-content .vip-box .head', function(e) {

        $('.post-content .vip-box .item .hinhthucdang').prop("checked", false);
        $('.post-content .vip-box .item .over').show();
        $('.post-content .vip-box .item').removeClass('selected');
        $('.post-content .vip-box .item select').addClass('nonselected');

        $(this).parents('.item').addClass('selected');
        $(this).parents('.item').find('.over').hide();
        $(this).parents('.item').find('.hinhthucdang').prop("checked", true);
        $(this).parents('.item').find('select').removeClass('nonselected');

        var hinhthucdang = $(this).parents('.item').find('.hinhthucdang').val();
        if (hinhthucdang == 0) {
            $('.post-content .vip-box .vipexplain').addClass('nonselected');
        } else {
            $('.post-content .vip-box .vipexplain').removeClass('nonselected');
            LaySoDuTaiKhoan();
        }
    });
    $("body").on('change', '.post-content .vip-box .vipperdayform .viptype', function(e) {
        var vipTypeId = $(this).val();

        if (vipTypeId == "") {
            $('.post-content .vip-box .vipperdayform .chiphi').html("0");
            return;
        }
        vipTypeId = parseInt(vipTypeId);
        var dayNumber = parseInt($('.post-content .vip-box .vipperdayform .daynumber').val());
        var price = parseInt($(".listviphidden #vip" + vipTypeId).attr("priceday"));
        //tính chi phí
        $('.post-content .vip-box .vipperdayform .chiphi').html(FormatNumberWithDot(price * dayNumber));
    });

    $("body").on('change', '.post-content .vip-box .vipperdayform .daynumber', function(e) {
        var vipTypeId = $('.post-content .vip-box .vipperdayform .viptype').val();

        if (vipTypeId == "") {
            $('.post-content .vip-box .vipperdayform .chiphi').html("0");
            return;
        }
        var dayNumber = parseInt($(this).val());
        vipTypeId = parseInt(vipTypeId);
        var price = parseInt($(".listviphidden #vip" + vipTypeId).attr("priceday"));
        //tính chi phí
        $('.post-content .vip-box .vipperdayform .chiphi').html(FormatNumberWithDot(price * dayNumber));
    });
    $("body").on('click', '.post-content .spfee', function(e) {
        $('.post-content #free').removeAttr('checked');
        $('.post-content #fee').attr('checked', 'checked');
    });
    $("body").on('click', '.post-content .spfree', function(e) {
        $('.post-content #fee').removeAttr('checked');
        $('.post-content #free').attr('checked', 'checked');
    });
    $("body").on('click', '.post-content .dvagree span', function(e) {
        if ($('.post-content #agree').is(':checked')) {
            $('.post-content #agree').removeAttr('checked');
        } else {
            $('.post-content #agree').attr('checked', 'checked');
        }
    });
});

function CaculateFileSize() {

    if (GetImageSize() > 29)
        alert("Dung lượng ảnh quá lớn vui lòng xóa bớt hoặc giảm dung lượng trước khi up hình");
}

function GetImageSize() {
    var size = 0;
    $(".post-content .upload-image-box .inputimage").each(function(index) {
        if (this.files[0] != null)
            size += this.files[0].size;
    });
    size = size / (1024 * 1024);
    return size;
}

function LaySoDuTaiKhoan() {
     var url = "/handler/Handler.ashx?command=" + COMMAND_CHECK_LOGIN;
    var temp = $(this);
    $.post(url, function(isLogin) {
        var isValidate = true;
        //success
        if (isLogin == "1") {
            var url = "/publish/handler/Handler.ashx?command=" + USER_GET_BALANCE_OF_MEMBER;
            $.post(url, function(sodu) {
                $('.post-content .vip-box .sodutaikhoan .value').html(sodu);
            });
        } else {
            window.top.window.ShowLoginForm();
            ResetVIPBox();
        }
    });
}

//kiểm tra người dùng có đủ tiền để nâng cấp VIP ko
function KiemTraDuSoDuNangCapVIP(hinhthucdang){
    if(hinhthucdang == 0)
        return true;
    var sodu = parseInt($('.post-content .vip-box .sodutaikhoan .value').html().replace(/\./g, ""));//xoa dấm chấm
    var chiphi = 0;
    if (hinhthucdang == 1) {
        chiphi = parseInt($('.post-content .vip-box .vipperdayform .chiphi').html().replace(/\./g, ""));//xoa dấm chấm;
    }
    if (hinhthucdang == 2) {
        var vipTypeId = $('.post-content .vip-box .vippermonthform .viptype').val();
        chiphi = parseInt($(".listviphidden #vip" + vipTypeId).attr("pricemonth"));
    }
    if (sodu - chiphi >= 0)
        return true;
    return false;
}
function AddProperty() {
    if (GetImageSize() > 29) {
        alert("Dung lượng ảnh quá lớn vui lòng xóa bớt hoặc giảm dung lượng trước khi up hình");
        return;
    }

    $('.left .loading').show();
    var url = "/handler/Handler.ashx?command=" + COMMAND_CHECK_LOGIN;
    var temp = $(this);
    $.post(url, function(isLogin) {
        var isValidate = true;
        //success
        if (isLogin == "1") {
            var matin = $('.post-content input#matin').val();
            var status = 1; //thêm mới
            if (matin != "")
                status = 2; //cập nhật
            var url = "/publish/handler/Handler.ashx?command=" + USER_CHECK_ALLOW_POST + "&status=" + status;
            $.post(url, function(allowPost) {
                if (allowPost != 1) {
                    ShowPostError("Xảy ra lỗi", "Tài khoản của bạn <b style='color:red'>chưa được chứng thực</b>, bạn chỉ có thể đăng được 2 tin. Xem <a href='/huong-dan/huong-dan-xac-thuc-tai-khoan' target='_blank' style='color:blue'>hướng dẫn</a> chứng thực tài khoản.");
                    return;
                }
                $('.post-content *').removeClass('fielderror');
                if (!$('.post-content #agree').is(':checked')) {
                    $('.post-content .dvagree').addClass('fielderror');
                    $('.post-content #agree').focus();
                    ShowPostError("Xảy ra lỗi", 'Bạn chưa cam kết thông tin mô tả về tài sản là đúng sự thật. Hãy đánh dấu vào ô "Tôi cam kết thông tin mô tả ..."');
                    return;
                }

                $('.post-content .require').hide();
                var tieude = $('.post-content input#tieude').val();
                $('.post-content textarea#noidung').val($('.post-content textarea#noidung').val().replace(/</g, "").replace(/>/g, ""));
                var noidung = $('.post-content textarea#noidung').val();
                var loaitin = $('.post-content select#loaitin').val();
                var loaibds = $('.post-content select#loaibds').val();
                var tinh = $('.post-content select#tinh').val();
                var huyen = $('.post-content select#huyen').val();
                var phuong = $('.post-content select#phuong').val();
                var duong = $('.post-content select#duong').val();
                var duan = $('.post-content select#project').val();
                var dientich = $('.post-content input#dientich').val().replace(/\s/g, ""); //xóa khoảng trắng
                $('.post-content input#dientich').val(dientich); //set lại giá trị
                var gia = $('.post-content input#gia').val().replace(/\s/g, ""); //xóa khoảng trắng
                $('.post-content input#gia').val(gia); //set lại giá trị
                var nguoilienhe = $('.post-content input#lienhe').val();
                var sodienthoai = $('.post-content input#dienthoai').val();
                var chieungang = $('.post-content input#chieungang').val();
                var chieudai = $('.post-content input#chieudai').val();
                var duongrong = $('.post-content input#duongrong').val();
                var solau = $('.post-content input#solau').val();
                var sophongngu = $('.post-content input#sophongngu').val();
                var captcha = $('.post-content input#captcha').val();
                var errorMessage = "";
                var isValidate = true;

                //validate
                if (tieude.length < 30 || tieude.length > 90) {
                    $('.post-content input#tieude').parents('.value').find('.require').html('Tiêu đề từ 30 - 90 ký tự').show();
                    $('.post-content input#tieude').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#tieude').focus()
                    }
                    errorMessage += "- Tiêu đề từ 30 - 90 ký tự.</br>";
                }
                if (noidung.length < 200 || noidung.length > 40000) {
                    $('.post-content textarea#noidung').parents('.value').find('.require').html('Nội dung từ 200 - 40000 ký tự. Nên mô tả chi tiết về tài sản, vị trí, tiện ích,...').show();
                    $('.post-content textarea#noidung').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content textarea#noidung').focus()
                    }
                    errorMessage += "- Nội dung từ 200 - 40000 ký tự.</br>";
                }
                if (loaitin == "0") {
                    $('.post-content select#loaitin').parents('.value').find('.require').html('Chọn loại tin').show();
                    $('.post-content select#loaitin').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content select#loaitin').focus()
                    }
                    errorMessage += "- Chưa chọn loại tin.</br>";
                }
                if (loaibds == 0) {
                    $('.post-content select#loaibds').parents('.value').find('.require').html('Chọn loại BĐS').show();
                    $('.post-content select#loaibds').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content select#loaibds').focus();
                    }
                    errorMessage += "- Chưa chọn loại BĐS.</br>";
                }
                if (tinh == 0) {
                    $('.post-content select#tinh').parents('.value').find('.require').html('Chọn tỉnh').show();
                    $('.post-content select#tinh').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content select#tinh').focus();
                    }
                    errorMessage += "- Chưa chọn tỉnh.</br>";
                }
                if (huyen == 0) {
                    $('.post-content select#huyen').parents('.value').find('.require').html('Chọn huyện').show();
                    $('.post-content select#huyen').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content select#huyen').focus();
                    }
                    errorMessage += "- Chưa chọn huyện.</br>";
                }
                if (phuong == '') {
                    $('.post-content select#phuong').parents('.value').find('.require').html('Chọn phường/xã').show();
                    $('.post-content select#phuong').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content select#phuong').focus();
                    }
                    errorMessage += "- Chưa chọn phường/xã.</br>";
                }
                if (duong == '') {
                    $('.post-content select#duong').parents('.value').find('.require').html('Chọn đường/phố').show();
                    $('.post-content .street-acp-box input').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content .street-acp-box input').focus();
                    }
                    errorMessage += "- Chưa chọn đường/phố.</br>";
                }
                if (loaibds == 4) {   //căn hộ             
                    if (duan == '') {
                        $('.post-content select#project').parents('.value').find('.require').html('Chọn dự án').show();
                        $('.post-content .project-acp-box input').addClass('fielderror');
                        isValidate = false;
                        if (errorMessage == "") {
                            $('.post-content .project-acp-box input').focus();
                        }
                        errorMessage += "- Chưa chọn dự án.</br>";
                    }
                }

                if ($('.post-content #kxd').is(':checked')) {
                    dientich = "0";

                } else {
                    if (dientich.length == 0) {
                        $('.post-content input#dientich').parents('.value').find('.require').html('Nhập diện tích hoặc diện tích SD').show();
                        $('.post-content input#dientich').addClass('fielderror');
                        isValidate = false;
                        if (errorMessage == "") {
                            $('.post-content input#dientich').focus();
                        }
                        errorMessage += "- Nhập diện tích/ diện tích SD.</br>";
                    } else {
                        if (!isNumber(dientich)) {
                            $('.post-content input#dientich').parents('.value').find('.require').html('Chỉ nhập số nguyên không chứa dấu (, .)').show();
                            $('.post-content input#dientich').addClass('fielderror');
                            isValidate = false;
                            if (errorMessage == "") {
                                $('.post-content input#dientich').focus();
                            }
                            errorMessage += "- Diện tích chỉ nhập số nguyên không chứa dấu (, .).</br>";
                        }
                    }
                }

                if ($('.post-content #thoathuan').is(':checked')) {
                    gia = 0;
                } else {
                    if (gia.length == 0) {
                        $('.post-content input#gia').parents('.value').find('.require').html('Nhập giá tiền').show();
                        $('.post-content input#gia').addClass('fielderror');
                        isValidate = false;
                        if (errorMessage == "") {
                            $('.post-content input#gia').focus();
                        }
                        errorMessage += "- Chưa nhập giá tiền.</br>";
                    } else {
                        if (!isNumber(gia)) {
                            $('.post-content input#gia').parents('.value').find('.require').html('Chỉ nhập số nguyên không chứa dấu (, .)').show();
                            $('.post-content input#gia').addClass('fielderror');
                            isValidate = false;
                            if (errorMessage == "") {
                                $('.post-content input#gia').focus();
                            }
                            errorMessage += "- Số tiền chỉ nhập số nguyên không chứa dấu (, .).</br>";
                        }
                    }
                }
                if (nguoilienhe.length == 0) {
                    $('.post-content input#lienhe').parents('.value').find('.require').html('Nhập tên người liên hệ').show();
                    $('.post-content input#lienhe').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#lienhe').focus();
                    }
                    errorMessage += "- Nhập tên người liên hệ.</br>";
                }

                if (sodienthoai.length == 0) {
                    $('.post-content input#dienthoai').parents('.value').find('.require').html('Nhập số điện thoại').show();
                    $('.post-content input#dienthoai').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#dienthoai').focus();
                    }
                    errorMessage += "- Nhập số điện thoại.</br>";
                }
                if (solau.length > 0 && !isNumber(solau)) {
                    $('.post-content input#solau').parents('.value').find('.require').html('Chỉ nhập số nguyên không chứa dấu (, .)').show();
                    $('.post-content input#solau').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#solau').focus();
                    }
                    errorMessage += "- Số lầu chỉ nhập số nguyên không chứa dấu (, .).</br>";
                }
                if (sophongngu.length > 0 && !isNumber(sophongngu)) {
                    $('.post-content input#sophongngu').parents('.value').find('.require').html('Chỉ nhập số nguyên không chứa dấu (, .)').show();
                    $('.post-content input#sophongngu').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#sophongngu').focus();
                    }
                    errorMessage += "- Số phòng chỉ nhập số nguyên không chứa dấu (, .).</br>";
                }
                if ((loaibds == 1 || loaibds == 2 || loaibds == 3 || loaibds == 9 || loaibds == 10 || loaibds == 11 || loaibds == 12 || loaibds == 13) && $.trim(duongrong) === "") {
                    $('.post-content input#duongrong').parents('.value').find('.require').html('Chưa nhập chiều rộng đường').show();
                    $('.post-content input#duongrong').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#duongrong').focus();
                    }
                    errorMessage += "- Chưa nhập chiều rộng đường trước nhà</br>";
                }
                if ((loaibds == 1 || loaibds == 2 || loaibds == 3 || loaibds == 4) && $.trim(sophongngu) === "") {
                    $('.post-content input#sophongngu').parents('.value').find('.require').html('Chưa nhập số phòng ngủ').show();
                    $('.post-content input#sophongngu').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#sophongngu').focus();
                    }
                    errorMessage += "- Chưa nhập số phòng ngủ</br>";
                }
                if ((loaibds == 1 || loaibds == 2 || loaibds == 3 || loaibds == 4) && $.trim(solau) === "") {
                    $('.post-content input#solau').parents('.value').find('.require').html('Nhập số 0 nếu không có lầu').show();
                    $('.post-content input#solau').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#solau').focus();
                    }
                    errorMessage += "- Chưa nhập số lầu</br>";
                }
                
                if (captcha == '') {
                    $('.post-content input#captcha').parents('.row').find('.require').html('Chưa nhập mã an toàn').show();
                    $('.post-content input#captcha').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#captcha').focus();
                    }
                    errorMessage += "- Chưa nhập mã an toàn.</br>";
                }

                var hinhthucdang = $('.post-content .vip-box .hinhthucdang:checked').val();
                if (hinhthucdang == 1) {
                    var vipTypeDay = $('.post-content .vip-box .vipperdayform .viptype').val();
                    var dayNumber = $('.post-content .vip-box .vipperdayform .daynumber').val();
                    if (vipTypeDay == "") {
                        $('.post-content .vip-box .vipperdayform .viptype').addClass('fielderror');
                        isValidate = false;
                        if (errorMessage == "") {
                            $('.post-content .vip-box .vipperdayform .viptype').focus();
                        }
                        errorMessage += "- Chưa chọn loại VIP.</br>";
                    }
                    if (dayNumber == "0") {
                        $('.post-content .vip-box .vipperdayform .daynumber').addClass('fielderror');
                        isValidate = false;
                        if (errorMessage == "") {
                            $('.post-content .vip-box .vipperdayform .daynumber').focus();
                        }
                        errorMessage += "- Chưa chọn số ngày VIP.</br>";
                    }
                    if (vipTypeDay != "" && dayNumber != 0) {
                        if (!KiemTraDuSoDuNangCapVIP(hinhthucdang)) {
                            $('.post-content .vip-box .vipperdayform .viptype').addClass('fielderror');
                            $('.post-content .vip-box .vipperdayform .daynumber').addClass('fielderror');
                            isValidate = false;
                            if (errorMessage == "") {
                                $('.post-content .vip-box .vipperdayform .daynumber').focus();
                            }
                            errorMessage += "- Không đủ số tiền để nâng cấp VIP.</br>";
                        }
                    }
                }
                if (hinhthucdang == 2) {
                    if ($('.post-content .vip-box .vippermonthform .viptype').val() == "") {
                        $('.post-content .vip-box .vippermonthform .viptype').addClass('fielderror');
                        isValidate = false;
                        if (errorMessage == "") {
                            $('.post-content .vip-box .vippermonthform .viptype').focus();
                        }
                        errorMessage += "- Chưa chọn loại VIP.</br>";
                    } else {
                        if (!KiemTraDuSoDuNangCapVIP(hinhthucdang)) {
                            $('.post-content .vip-box .vippermonthform .viptype').addClass('fielderror');
                            isValidate = false;
                            if (errorMessage == "") {
                                $('.post-content .vip-box .vippermonthform .viptype').focus();
                            }
                            errorMessage += "- Không đủ số tiền để nâng cấp VIP.</br>";
                        }
                    }
                }

                if (isValidate) {
                    var url = "/handler/Handler.ashx?command=" + COMMAND_CHECK_CAPTCHA + "&captcha=" + captcha; ;
                    $.post(url, function(isMatch) {
                        if (isMatch == 1) {
                            $(".post-content #form1").submit();
                        } else {
                            alert("Mã an toàn không đúng.");
                            $('.post-content img.captchagenerator').attr("src", "/CaptchaGenerator.ashx?t=" + Math.floor((Math.random() * 100000) + 1)); //reload captcha
                            $('.post-content input#captcha').parents('.row').find('.require').html('Mã an toàn không đúng').show();
                            $('.post-content input#captcha').addClass('fielderror');
                            $('.post-content input#captcha').focus();
                            $('.left .loading').hide();
                        }
                    });

                } else {
                    ShowPostError("Xảy ra lỗi", errorMessage);
                }
            });
        } else {
            ShowLoginForm();
            $('.left .loading').hide();
        }
    });
}

function finish_post(result, message) {

    if (result == 1) {
        $('.left .loading').hide();  
        $('#form1').hide(); //hide form
        $('.submit-result-box').show();
    } else 
    {
        if (message != "")
            alert(message);
        else
            alert("Thông tin nhập không hợp lệ.");
        $('.left .loading').hide();
        //reload captcha
        $('.post-content img.captchagenerator').attr("src", "/CaptchaGenerator.ashx?t=" + Math.floor((Math.random() * 100000) + 1));        
    }
    $('html, body').animate({ scrollTop: 0 }, 0);
}
function PostOtherNews() {
    var url = "/publish/form/PropertyAddForm.aspx";
    GetPostForm(url);    
}

function GetPostForm(url) {
    $('.submit-result-box').hide();
    $('.left .loading').show();
    $.post(url, function(data) {
        $('.content-box').html(data).show();
        LaySoDuTaiKhoan();
        $('.left .loading').hide();
        LoadPropertyInformPopup();
        ShowMap();
    });
}
function ShowMap() {
    var mapLat = $('#map_lat').val();
    var mapLng = $('#map_lng').val();
    var mapFlag = $('.map-flag').val();
    var webversion = $('.web-version').val();
    if (mapFlag == "1" && webversion == "1") {
        InitMap(mapLat, mapLng);
    } else {
        if (mapFlag == "2" && webversion == "2") {
            InitMap(mapLat, mapLng);
        } else {
            if (mapFlag == "3") {
                InitMap(mapLat, mapLng);
            } else {
                $('.bando').hide();
            }
        }
    }
}
function GetAdvancePostForm(url) {
    $('.left .loading').show();    
    $('.content-box').html('<iframe src="' + url + '" frameborder="0" scrolling="no" id="myFrame"></iframe>');
    $('.left .loading').hide();
}
function ResetForm() {
    $('.post-content .require').hide();
    $('.post-content *').removeClass('fielderror');
    $("#form1")[0].reset();
    $('.post-content .price_text').html('');
    $('.post-content .divThumbnail').html("");
    ResetVIPBox();    
}
function ResetVIPBox() {
    $('.post-content .vip-box .item .hinhthucdang').prop("checked", false);
    $('.post-content .vip-box .item .over').show();
    $('.post-content .vip-box .item').removeClass('selected');
    $('.post-content .vip-box .item select').addClass('nonselected');

    $('.post-content .vip-box .item:nth-child(1)').addClass('selected');
    $('.post-content .vip-box .item:nth-child(1)').find('.over').hide();
    $('.post-content .vip-box .item:nth-child(1)').find('.hinhthucdang').prop("checked", true);
    $('.post-content .vip-box .item:nth-child(1)').removeClass('nonselected');
    $('.post-content .vip-box .vipexplain').addClass('nonselected');
    $('.post-content .vip-box .chiphi').html('0');
}
//ADVANCE
function AddAdvanceProperty() {
    $('.propertyaddform .loading').show();
    var url = "/handler/Handler.ashx?command=" + COMMAND_CHECK_LOGIN;
    $.post(url, function(isLogin) {
        var isValidate = true;
        //success
        if (isLogin == "1") {
            var matin = $('.post-content input#matin').val();
            var status = 1; //thêm mới
            if (matin != "")
                status = 2; //cập nhật
            var url = "/publish/handler/Handler.ashx?command=" + USER_CHECK_ALLOW_POST + "&status=" + status;
            $.post(url, function(allowPost) {
                if (allowPost != 1) {
                    ShowAdvancePostError("Xảy ra lỗi", "Tài khoản của bạn <b style='color:red'>chưa được chứng thực</b>, bạn chỉ có thể đăng được 2 tin. Xem <a href='/huong-dan/huong-dan-xac-thuc-tai-khoan' style='color:blue'>hướng dẫn</a> chứng thực tài khoản.");
                    return;
                }
                $('.post-content *').removeClass('fielderror');
                if (!$('.post-content #agree').is(':checked')) {
                    $('.post-content .dvagree').addClass('fielderror');
                    $('.post-content #agree').focus();
                    ShowAdvancePostError("Xảy ra lỗi", 'Bạn chưa cam kết thông tin mô tả về tài sản là đúng sự thật. Hãy đánh dấu vào ô "Tôi cam kết thông tin mô tả ..."');
                    return;
                }
                $('.post-content .require').hide();
                var noidung = $('.post-content').find('iframe').contents().find('body').html();
                var tieude = $('.post-content input#tieude').val();
                var thumbnail = $('.post-content input#thumbnail').val();
                var tomtat = $('.post-content textarea#tomtat').val();
                var loaitin = $('.post-content select#loaitin').val();
                var loaibds = $('.post-content select#loaibds').val();
                var tinh = $('.post-content select#tinh').val();
                var huyen = $('.post-content select#huyen').val();
                var phuong = $('.post-content select#phuong').val();
                var duong = $('.post-content select#duong').val();
                var duan = $('.post-content select#project').val();
                var dientich = $('.post-content input#dientich').val().replace(/\s/g, ""); //xóa khoảng trắng
                $('.post-content input#dientich').val(dientich); //set lại giá trị
                var gia = $('.post-content input#gia').val().replace(/\s/g, ""); //xóa khoảng trắng
                $('.post-content input#gia').val(gia); //set lại giá trị
                var nguoilienhe = $('.post-content input#lienhe').val();
                var sodienthoai = $('.post-content input#dienthoai').val();
                var chieungang = $('.post-content input#chieungang').val();
                var chieudai = $('.post-content input#chieudai').val();
                var duongrong = $('.post-content input#duongrong').val();
                var solau = $('.post-content input#solau').val();
                var sophongngu = $('.post-content input#sophongngu').val();
                var captcha = $('.post-content input#captcha').val();
                var errorMessage = "";
                var isValidate = true;

                //validate
                if (tieude.length < 30 || tieude.length > 90) {
                    $('.post-content input#tieude').parents('.value').find('.require').html('Tiêu đề từ 30 - 90 ký tự').show();
                    $('.post-content input#tieude').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#tieude').focus()
                    }
                    errorMessage += "- Tiêu đề từ 30 - 90 ký tự.</br>";
                }
                if (thumbnail.length < 10) {
                    $('.post-content input#thumbnail').parents('.value').find('.require').html('Chưa có ảnh').show();
                    $('.post-content input#thumbnail').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#thumbnail').focus()
                    }
                    errorMessage += "- Chưa có ảnh.</br>";
                }
                if (tomtat.length < 100 || tomtat.length > 200) {
                    $('.post-content textarea#tomtat').parents('.value').find('.require').html('Tóm tắt phải từ 100 - 200 ký tự').show();
                    $('.post-content textarea#tomtat').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content textarea#tomtat').focus()
                    }
                    errorMessage += "- Tóm tắt phải từ 100 - 200 ký tự.</br>";
                }
                if (noidung.length > 40000) {
                    $('.post-content textarea#noidung').parents('.value').find('.require').html('Nội dung phải nhỏ hơn 40.000 ký tự.').show();
                    $('.post-content textarea#noidung').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content textarea#tomtat').focus()
                    }
                    errorMessage += "- Nội dung phải nhỏ hơn 40.000 ký tự.</br>";
                }
                if (loaitin == 0) {
                    $('.post-content select#loaitin').parents('.value').find('.require').html('Chọn loại tin').show();
                    $('.post-content select#loaitin').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content select#loaitin').focus()
                    }
                    errorMessage += "- Chọn loại tin.</br>";
                }
                if (loaibds == 0) {
                    $('.post-content select#loaibds').parents('.value').find('.require').html('Chọn loại BĐS').show();
                    $('.post-content select#loaibds').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content select#loaibds').focus();
                    }
                    errorMessage += "- Chọn loại BĐS.</br>";
                }
                if (tinh == 0) {
                    $('.post-content select#tinh').parents('.value').find('.require').html('Chọn tỉnh').show();
                    $('.post-content select#tinh').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content select#tinh').focus();
                    }
                    errorMessage += "- Chọn tỉnh.</br>";
                }
                if (huyen == 0) {
                    $('.post-content select#huyen').parents('.value').find('.require').html('Chọn huyện').show();
                    $('.post-content select#huyen').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content select#huyen').focus();
                    }
                    errorMessage += "- Chọn huyện.</br>";
                }
                if (phuong == '') {
                    $('.post-content select#phuong').parents('.value').find('.require').html('Chọn phường/xã').show();
                    $('.post-content select#phuong').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content select#phuong').focus();
                    }
                    errorMessage += "- Chọn phường/xã.</br>";
                }
                if (duong == '') {
                    $('.post-content select#duong').parents('.value').find('.require').html('Chọn đường/phố').show();
                    $('.post-content .street-acp-box input').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content .street-acp-box input').focus();
                    }
                    errorMessage += "- Chọn đường/phố.</br>";
                }
                if (loaibds == 4) {   //căn hộ             
                    if (duan == '') {
                        $('.post-content select#project').parents('.value').find('.require').html('Chọn dự án').show();
                        $('.post-content .project-acp-box input').addClass('fielderror');
                        isValidate = false;
                        if (errorMessage == "") {
                            $('.post-content .project-acp-box input').focus();
                        }
                        errorMessage += "- Chọn dự án.</br>";
                    }
                }

                if ($('.post-content #kxd').is(':checked')) {
                    dientich = "0";

                } else {
                    if (dientich.length == 0) {
                        $('.post-content input#dientich').parents('.value').find('.require').html('Nhập diện tích hoặc diện tích SD').show();
                        $('.post-content input#dientich').addClass('fielderror');
                        isValidate = false;
                        if (errorMessage == "") {
                            $('.post-content input#dientich').focus();
                        }
                        errorMessage += "- Nhập diện tích/ diện tích SD.</br>";
                    } else {
                        if (!isNumber(dientich)) {
                            $('.post-content input#dientich').parents('.value').find('.require').html('Chỉ nhập số nguyên không chứa dấu (, .)').show();
                            $('.post-content input#dientich').addClass('fielderror');
                            isValidate = false;
                            if (errorMessage == "") {
                                $('.post-content input#dientich').focus();
                            }
                            errorMessage += "- Diện tích chỉ nhập số nguyên không chứa dấu (, .).</br>";
                        }
                    }
                }

                if ($('.post-content #thoathuan').is(':checked')) {
                    gia = 0;
                } else {
                    if (gia.length == 0) {
                        $('.post-content input#gia').parents('.value').find('.require').html('Nhập giá tiền').show();
                        $('.post-content input#gia').addClass('fielderror');
                        isValidate = false;
                        if (errorMessage == "") {
                            $('.post-content input#gia').focus();
                        }
                        errorMessage += "- Chưa nhập giá tiền.</br>";
                    } else {
                        if (!isNumber(gia)) {
                            $('.post-content input#gia').parents('.value').find('.require').html('Chỉ nhập số nguyên không chứa dấu (, .)').show();
                            $('.post-content input#gia').addClass('fielderror');
                            isValidate = false;
                            if (errorMessage == "") {
                                $('.post-content input#gia').focus();
                            }
                            errorMessage += "- Số tiền chỉ nhập số nguyên không chứa dấu (, .).</br>";
                        }
                    }
                }
                if (nguoilienhe.length == 0) {
                    $('.post-content input#lienhe').parents('.value').find('.require').html('Nhập tên người liên hệ').show();
                    $('.post-content input#lienhe').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#lienhe').focus();
                    }
                    errorMessage += "- Nhập tên người liên hệ.</br>";
                }

                if (sodienthoai.length == 0) {
                    $('.post-content input#dienthoai').parents('.value').find('.require').html('Nhập số điện thoại').show();
                    $('.post-content input#dienthoai').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#dienthoai').focus();
                    }
                    errorMessage += "- Nhập số điện thoại.</br>";
                }
                if (solau.length > 0 && !isNumber(solau)) {
                    $('.post-content input#solau').parents('.value').find('.require').html('Chỉ nhập số nguyên không chứa dấu (, .)').show();
                    $('.post-content input#solau').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#solau').focus();
                    }
                    errorMessage += "- Số lầu chỉ nhập số nguyên không chứa dấu (, .).</br>";
                }
                if (sophongngu.length > 0 && !isNumber(sophongngu)) {
                    $('.post-content input#sophongngu').parents('.value').find('.require').html('Chỉ nhập số nguyên không chứa dấu (, .)').show();
                    $('.post-content input#sophongngu').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#sophongngu').focus();
                    }
                    errorMessage += "- Số phòng chỉ nhập số nguyên không chứa dấu (, .).</br>";
                }
                if ((loaibds == 1 || loaibds == 2 || loaibds == 3 || loaibds == 9 || loaibds == 10 || loaibds == 11 || loaibds == 12 || loaibds == 13) && $.trim(duongrong) === "") {
                    $('.post-content input#duongrong').parents('.value').find('.require').html('Chưa nhập chiều rộng đường').show();
                    $('.post-content input#duongrong').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#duongrong').focus();
                    }
                    errorMessage += "- Chưa nhập chiều rộng đường trước nhà</br>";
                }
                if ((loaibds == 1 || loaibds == 2 || loaibds == 3 || loaibds == 4) && $.trim(sophongngu) === "") {
                    $('.post-content input#sophongngu').parents('.value').find('.require').html('Chưa nhập số phòng ngủ').show();
                    $('.post-content input#sophongngu').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#sophongngu').focus();
                    }
                    errorMessage += "- Chưa nhập số phòng ngủ</br>";
                }
                if ((loaibds == 1 || loaibds == 2 || loaibds == 3 || loaibds == 4) && $.trim(solau) === "") {
                    $('.post-content input#solau').parents('.value').find('.require').html('Nhập số 0 nếu không có lầu').show();
                    $('.post-content input#solau').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#solau').focus();
                    }
                    errorMessage += "- Chưa nhập số lầu</br>";
                }
                if (captcha == '') {
                    $('.post-content input#captcha').parents('.row').find('.require').html('Chưa nhập mã an toàn').show();
                    $('.post-content input#captcha').addClass('fielderror');
                    isValidate = false;
                    if (errorMessage == "") {
                        $('.post-content input#captcha').focus();
                    }
                    errorMessage += "- Chưa nhập mã an toàn.</br>";
                }

                var hinhthucdang = $('.post-content .vip-box .hinhthucdang:checked').val();
                if (hinhthucdang == 1) {
                    var vipTypeDay = $('.post-content .vip-box .vipperdayform .viptype').val();
                    var dayNumber = $('.post-content .vip-box .vipperdayform .daynumber').val();
                    if (vipTypeDay == "") {
                        $('.post-content .vip-box .vipperdayform .viptype').addClass('fielderror');
                        isValidate = false;
                        if (errorMessage == "") {
                            $('.post-content .vip-box .vipperdayform .viptype').focus();
                        }
                        errorMessage += "- Chưa chọn loại VIP.</br>";
                    }
                    if (dayNumber == "0") {
                        $('.post-content .vip-box .vipperdayform .daynumber').addClass('fielderror');
                        isValidate = false;
                        if (errorMessage == "") {
                            $('.post-content .vip-box .vipperdayform .daynumber').focus();
                        }
                        errorMessage += "- Chưa chọn số ngày VIP.</br>";
                    }
                    if (vipTypeDay != "" && dayNumber != 0) {
                        if (!KiemTraDuSoDuNangCapVIP(hinhthucdang)) {
                            $('.post-content .vip-box .vipperdayform .viptype').addClass('fielderror');
                            $('.post-content .vip-box .vipperdayform .daynumber').addClass('fielderror');
                            isValidate = false;
                            if (errorMessage == "") {
                                $('.post-content .vip-box .vipperdayform .daynumber').focus();
                            }
                            errorMessage += "- Không đủ số tiền để nâng cấp VIP.</br>";
                        }
                    }
                }
                if (hinhthucdang == 2) {
                    if ($('.post-content .vip-box .vippermonthform .viptype').val() == "") {
                        $('.post-content .vip-box .vippermonthform .viptype').addClass('fielderror');
                        isValidate = false;
                        if (errorMessage == "") {
                            $('.post-content .vip-box .vippermonthform .viptype').focus();
                        }
                        errorMessage += "- Chưa chọn loại VIP.</br>";
                    } else {
                        if (!KiemTraDuSoDuNangCapVIP(hinhthucdang)) {
                            $('.post-content .vip-box .vippermonthform .viptype').addClass('fielderror');
                            isValidate = false;
                            if (errorMessage == "") {
                                $('.post-content .vip-box .vippermonthform .viptype').focus();
                            }
                            errorMessage += "- Không đủ số tiền để nâng cấp VIP.</br>";
                        }
                    }
                }

                if (isValidate) {
                    var url = "/handler/Handler.ashx?command=" + COMMAND_CHECK_CAPTCHA + "&captcha=" + captcha; ;
                    $.post(url, function(isMatch) {
                        if (isMatch == 1) {
                            $(".post-content #form1").submit();
                        } else {
                            alert("Mã an toàn không đúng.");
                            $('.post-content img.captchagenerator').attr("src", "/CaptchaGenerator.ashx?t=" + Math.floor((Math.random() * 100000) + 1)); //reload captcha
                            $('.post-content input#captcha').parents('.row').find('.require').html('Mã an toàn không đúng').show();
                            $('.post-content input#captcha').addClass('fielderror');
                            $('.propertyaddform .loading').hide();
                        }
                    });
                } else {
                    ShowAdvancePostError("Xảy ra lỗi", errorMessage);
                    //alert(errorMessage);
                    //$('.propertyaddform .loading').hide();
                    return false;
                }
            });

        } else {
            window.top.window.ShowLoginForm();
            $('.propertyaddform .loading').hide();
            return false;
        }
    });
}
function AdvancePostFinish(result, message) {
    $('#myFrame').contents().find('.loading').hide();
    if (result == 1) {
        $('#myFrame').contents().find('#form1').hide(); //hide form
        $('#myFrame').contents().find('.submit-result-box').show();
    } else {
        alert(message);
        //reload captcha
        $('#myFrame').contents().find('.post-content img.captchagenerator').attr("src", "/CaptchaGenerator.ashx?t=" + Math.floor((Math.random() * 100000) + 1));
    }
    $('html, body').animate({ scrollTop: 0 }, 0);
}
function ShowThumbnail() {
    var path = $('input.thumbnail').val();
    path = path.replace("images", "thumbnails");
    $('.thumbnail').val(path);
    $('.divThumbnail').html("<img src='" + path + "'/>");
}

function OpenUpload() {
    var newwindow = window.open('/upload.aspx', 'Upload', 'height=500,width=820,left=200,top=100,resizable=true,scrollbars=true,toolbar=yes,status=yes');
}

//====== END POST ========

function BackToDefault() {
    $('.content-box').html("");
    $('.manage-menu li ul li').removeClass('selected');
}

function UserGetDistricts() {

    var maTinh = $('.user-infor #tinh').val();
    if (maTinh == 0) {
        $('.user-infor #huyen').html("<option value='0'>------ Quận/Huyện</option>");
        return;
    }
    var url = "/handler/Handler.ashx?command=" + COMMAND_GET_DISTRICTS + "&matinh=" + maTinh;
    
    $.post(url).success(function(data) {
        $('.user-infor #huyen').html(data);
    }).error(function(data) {
        alert("Xảy ra lỗi!");
    });
}
function UserGetDistricts1() {

    var maTinh = $('.user-infor #tinh1').val();
    if (maTinh == 0) {
        $('.user-infor #huyen1').html("<option value='0'>------ Quận/Huyện</option>");
        return;
    }
    var url = "/handler/Handler.ashx?command=" + COMMAND_GET_DISTRICTS + "&matinh=" + maTinh;

    $.post(url).success(function(data) {
        $('.user-infor #huyen1').html(data);
    }).error(function(data) {
        alert("Xảy ra lỗi!");
    });
}


function ChangePassword() {

    var password = $('#password').val();
    var password1 = $('#password1').val();
    var password2 = $('#password2').val();
    if (password.length == 0) {
        alert("Chưa nhập mật khẩu cũ");
        return;
    }
    if (password1.length == 0) {
        alert("Chưa nhập mật khẩu mới");
        return;
    }
    if (password2.length == 0) {
        alert("Chưa xác nhận mật khẩu mới");
        return;
    }

    if (password1.length < 6 || password1.length > 30) {
        alert("Mật khẩu phải lớn hơn 6 và nhỏ hơn 30 ký tự");
        return;
    }
    if (password1 != password2) {
        alert("Mật khẩu mới phải giống nhau");
        return;
    }
    $('.loading').show();
    var url = "/publish/handler/Handler.ashx?command=" + USER_CHANGE_PASSWORD;
    $.post(url,
	    {
	        oldPassword: password,
	        newPassword: password1
	    }, function(data) {
	        var fields = data.split(/;/);
	        var type = fields[0];
	        var message = fields[1];
	        //success
	        if (type == "1") {
	            $('.change-password-box .result-message').html("<span class='success'>" + message + "</span");
	            //reset form
	            $('#password').val("");
	            $('#password1').val("");
	            $('#password2').val("");

	        } else {
	            $('.change-password-box .result-message').html("<span class='error'>" + message + "</span");
	        }
	        $('.change-password-box .result-message').show();
	        $('.loading').hide();
	    });
	}
	
	//USER UPDATE INFOR
	$(document).ready(function() {
	    $("body").on('click', '.user-infor .agent', function() {
	        var agent = $('.user-infor .agent:checked').val();
	        if (agent == 2)
	            $('.user-infor .row-agent').slideDown(0);
	        else
	            $('.user-infor .row-agent').slideUp(0);
	    });
	});

	
function UpdateUserInfor() {
    //validate
    var fullname = $('.user-infor #fullname').val();
    var email1 = $('.user-infor #email1').val();
    var email2 = $('.user-infor #email2').val();
    var province = $('.user-infor #tinh').val();
    var district = $('.user-infor #huyen').val();
    var phone1 = $('.user-infor #phone1').val();
    var phone2 = $('.user-infor #phone2').val();
    var avatar = $('.user-infor #avatar');
    var agent = $('.user-infor .agent:checked').val();
    $('.user-infor *').removeClass('fielderror');
    //user name
    if (fullname.length == 0) {
        alert("Chưa nhập họ tên");
        return;
    }

    var re = new RegExp("^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$");
    var m = re.exec(email1);

    if (m == null) {
        $('.user-infor #email1').addClass('fielderror');
        alert("Email không hợp lệ");
        return;
    }

    if (email1 != email2) {
        $('.user-infor #email1').addClass('fielderror');
        $('.user-infor #email2').addClass('fielderror');
        alert("Email được nhập phải giống nhau");
        return;
    }
    //check phone 1
    re = new RegExp("^[0-9]{8,12}$");
    m = re.exec(phone1);
    if (m == null) {
        $('.user-infor #phone1').addClass('fielderror');
        alert("Số điện thoại không hợp lệ");
        return;
    }
    if (phone2 != "") {
        //check phone 2
        re = new RegExp("^[0-9]{8,12}$");
        m = re.exec(phone2);
        if (m == null) {
            $('.user-infor #phone2').addClass('fielderror');
            alert("Số điện thoại không hợp lệ");
            return;
        }
        if (phone1 == phone2) {
            $('.user-infor #phone1').addClass('fielderror');
            $('.user-infor #phone2').addClass('fielderror');
            alert("Số điện thoại phải khác nhau");
            return;
        }
    }
    if (agent == 2) {
        var loaibds = $('.user-infor #loaibds').val();
        var tinh1 = $('.user-infor #tinh1').val();
        var huyen1 = $('.user-infor #huyen1').val();
        var introduce = $('.user-infor #introduce').val();
        if (loaibds == '0;0') {
            $('.user-infor #loaibds').addClass('fielderror');
            alert("Chưa chọn loại BĐS môi giới");
            return;
        }
        if (tinh1 == 0) {
            $('.user-infor #tinh1').addClass('fielderror');
            alert("Chưa chọn tỉnh/thành trong hoạt động môi giới");
            return;
        }
        if (huyen1 == 0) {
            $('.user-infor #huyen1').addClass('fielderror');
            alert("Chưa chọn quận/huyện trong hoạt động môi giới");
            return;
        }
        if (introduce.length <= 100) {
            $('.user-infor #introduce').addClass('fielderror');
            alert("Giới thiệu về hoạt động môi giới phải lớn hơn 100 ký tự");
            return;
        }    
    }
    $('.loading').show();
    $('.user-infor-form').submit();  
    
}
function UpdateInforSuccess(data) {
    var fields = data.split(/;/);
    var type = fields[0];
    var message = fields[1];
    //success
    if (type == "1") {
        $('.user-infor .result-message').html("<span class='success'>" + message + "</span");

        var input = $('.user-infor .contact-infor .input-avatar')[0];
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.user-infor #avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    } else {
        $('.user-infor .result-message').html("<span class='error'>" + message + "</span");
    }
    $('.user-infor .result-message').show();
    GoTop(0);
    $('.loading').hide(); ;
}
//MOBILE CARD
function NapCard() {
    var cardtype = $('.mobilecardform #cardtype').val();
    var cardcode = $('.mobilecardform #cardcode').val();
    var cardserial = $('.mobilecardform #cardserial').val();
    if (cardtype.length == 0) {
        alert("Chưa chọn loại thẻ");
        return;
    }
    if (cardcode.length == 0) {
        alert("Chưa nhập mã thẻ");
        return;
    }
    if (cardserial.length == 0) {
        alert("Chưa nhập số serial");
        return;
    }

    $('.loading').show();
    var url = "/publish/handler/Handler.ashx?command=" + USER_NAP_CARD;
    $.post(url,
    {
        cardtype: cardtype,
        cardcode: cardcode,
        cardserial: cardserial
    }, function(data) {
        var fields = data.split(/;/);
        var type = fields[0];
        var message = fields[1];
        //success
        if (type == "1") {
            $('.mobilecardform .errormessage').html("<span style='color:blue'>" + message + "</span");
            $('.mobilecardform .errormessage').show();
            $('.mobilecardform #cardcode').val('');
            $('.mobilecardform #cardserial').val('');
        } else {
        $('.mobilecardform .errormessage').html("<span style='color:red'>" + message + "</span");
            $('.mobilecardform .errormessage').show(); ;
        }
        $('.loading').hide();
    });
}

function NapCard2() {
    var url = "/handler/Handler.ashx?command=" + COMMAND_CHECK_LOGIN;
    $.post(url, function(isLogin) {
        var isValidate = true;
        //success
        if (isLogin == "1") {
            var cardtype = $('.mobilecardform2 .cardtype').val();
            var cardcode = $('.mobilecardform2 .cardcode').val();
            var cardserial = $('.mobilecardform2 .cardserial').val();
            if (cardtype.length == 0) {
                alert("Chưa chọn loại thẻ");
                return;
            }
            if (cardcode.length == 0) {
                alert("Chưa nhập mã thẻ");
                return;
            }
            if (cardserial.length == 0) {
                alert("Chưa nhập số serial");
                return;
            }

            $('.mobilecardform2 .loading').show();
            var url = "/publish/handler/Handler.ashx?command=" + USER_NAP_CARD;
            $.post(url,
            {
                cardtype: cardtype,
                cardcode: cardcode,
                cardserial: cardserial
            }, function(data) {
                var fields = data.split(/;/);
                var type = fields[0];
                var message = fields[1];
                //success
                if (type == "1") {
                    $('.mobilecardform2 .errormessage').html("<span style='color:blue'>" + message + "</span");
                    $('.mobilecardform2 .errormessage').show();
                    $('.mobilecardform2 .cardcode').val('');
                    $('.mobilecardform2 .cardserial').val('');
                } else {
                    $('.mobilecardform2 .errormessage').html("<span style='color:red'>" + message + "</span");
                    $('.mobilecardform2 .errormessage').show(); ;
                }
                $('.mobilecardform2 .loading').hide();
            });
        } else {
            window.top.window.showForm("login-form");
        }
    });	    
}

//kiểm tra số tin không được duyệt
function LoadMemberInformPopup() {
    var url = "/publish/handler/Handler.ashx?command=" + USER_CHECK_MEMBER_INFORM_POPUP;
    $.post(url).success(function(data) {
        var total = parseInt(data);
        if (data >= 3) {
            var popup =
		        "Bạn có <b>" + total + "</b> tin không được duyệt. <br />" +
		        "Để tiếp tục đăng tin bạn phải <b>xóa</b> hoặc <b>sửa</b> các tin này. <br/>" +
		        "Tài khoản của bạn sẽ <b>bị khóa</b> nếu có hơn 10 tin không được duyệt. <br/>";
            ShowMemberPopup("Thông báo", popup);
        };
    }).error(function(data) {

    });
}

//kiểm tra số tin miễn phí được đăng - trang tin thường
function LoadPropertyInformPopup() {
    var matin = $('.post-content input#matin').val();
    if (matin == "") {
        var url = "/publish/handler/Handler.ashx?command=" + USER_CHECK_PROPERTY_INFORM_POPUP;
        $.post(url).success(function(data) {
            var notAllowPost = parseInt(data);
            if (notAllowPost == 1) {
                //xử lý VIP box
                SetDefaultVIPDay();
            };
        }).error(function(data) {

        });
    }
}

//kiểm tra số tin miễn phí được đăng  - đăng nâng cao thường
function LoadAddvancePropertyInformPopup() {
    var matin = $('.post-content input#matin').val();
    if (matin == "") {
        var url = "/publish/handler/Handler.ashx?command=" + USER_CHECK_PROPERTY_INFORM_POPUP;
        $.post(url).success(function(data) {
            var notAllowPost = parseInt(data);
            if (notAllowPost == 1) {
                /*var popup =
		            "Bạn đã đăng <span style='color:red'>2 tin</span> miễn phí trong ngày. <br />Để tiếp tục đăng tin bạn phải đăng tin <b>VIP</b> (<span style='font-style:italic'>tối thiểu <span style='color:blue'>1 ngày VIP</span>  (10.000đ), sau khi hết <b>VIP</b> tin sẽ vẫn được hiển thị như tin thường</span>) hoặc chờ qua ngày mai để tiếp tục đăng tin miễn phí.<br>";
                ShowAdvancePropertyPopup("Thông báo", popup);
                */
                //xử lý VIP box
                SetDefaultVIPDay();
            };
        }).error(function(data) {

        });
    }
}
function SetDefaultVIPDay() {
    $('.post-content .vip-box .item .regular-property-over').show(); //disable regular property
    $(".post-content .vip-box .item").removeClass('selected');
    $(".post-content .vip-box .item .hinhthucdang[value='1']").prop("checked", true); //chọn VIP mặc định
    $(".post-content .vip-box .vipperdayform .over").hide();
    $(".post-content .vip-box .vipperdayform").addClass('selected');
    $('.post-content .vip-box .vipexplain').removeClass('nonselected');
    $(".post-content .vip-box .vipperdayform").find('select').removeClass('nonselected');    
}
function ShowPostError(title, content) {
    ShowMemberPopup(title, content);
    $(".left .loading").show();
    $(".member-inform-popup .content").css('text-align', 'left');
}
function ShowAdvancePostError(title, content) {
    ShowMemberPopup(title, content);
    $(".propertyaddform .loading").show();
    $(".member-inform-popup .content").css('text-align', 'left');
    $(".member-inform-popup").css("top", ($(window.top.window).height() - $('.member-inform-popup').height()) / 2 + $(window.top.window).scrollTop() - 250 + "px");   
}
function ShowPropertyPopup(title, content) {
    ShowMemberPopup(title, content);
    $(".left .loading").show();
    $(".member-inform-popup .content").css('text-align', 'center');
    //$(".member-inform-popup").css("width", '400px');
}
function ShowAdvancePropertyPopup(title, content) {
    ShowMemberPopup(title, content);
    $(".propertyaddform .loading").show();
    $(".member-inform-popup .content").css('text-align', 'center');
    //$(".member-inform-popup").css("width", '400px');
    //$(".member-inform-popup .popup-focus").focus();
    $(".member-inform-popup").css("top", ($(window.top.window).height() - $('.member-inform-popup').height()) / 2 + $(window.top.window).scrollTop() - 200 + "px");
}
function ShowMemberPopup(title, content) {
    $(".member-inform-popup .head").html(title);
    $(".member-inform-popup .content").html(content);
    $(".member-inform-popup").show();
    $(".member-inform-popup").css("top", ($(window).height() - $('.member-inform-popup').height()) / 2 + $(window).scrollTop() + "px");
    
}
function CloseMemberForm() {
    $(".member-inform-popup").hide();
    $(".loading").hide();
}

function UserVerifyClick() {
    var url = "/publish/handler/Handler.ashx?command=" + USER_VERIFY_ACCOUNT;
    $.post(url, function(data) {
        var fields = data.split(/;/);
        var type = fields[0];
        var message = fields[1];
        //success
        if (type == "1") {
            $('.user-infor .result-message').html("<span class='success'>" + message + "</span");
            $('.user-infor #verify_status').html("<img src='/publish/img/verified.png'>");
            $('.inform-verify').remove();
        } else {
            $('.user-infor .result-message').html("<span class='error'>" + message + "</span");
        }
        $('.user-infor .result-message').show();
        $('.loading').hide();
        GoTop(0);
    });
}

function LoadRenewPackageList() {
    $('.manage-menu li ul li').removeClass('selected');
    $(this).addClass('selected');
    var url = "/publish/handler/Handler.ashx?command=15";
    $('.loading').show();
    $.post(url, function(data) {
        $('.content-box').html(data);
        $('.loading').hide();
    });
}

function BuyUp(upId) {
    $('.renew-package-list .loading').show();
    var r = confirm("Bạn có chắc muốn mua gói UP này ?");
    if (r == false) {
        $('.renew-package-list .loading').hide();
        return;
    }
    var url = "/publish/handler/Handler.ashx?command=" + USER_RENEW_PACKAGE_BUY + "&upid=" + upId;
    $.post(url).success(function(data) {
        var fields = data.split(/;/);
        var type = fields[0];
        var message = fields[1];
        //success
        if (type == "1") {
            $('.renew-package-list .inform-renew-box .message').html(message);
            $('.renew-package-list .inform-renew-box').show();
        } else {
            $('.renew-package-list .message').html("<div class='error'>" + message + ".</div>").show(); //hiển thị thông báo lỗi        
        }
        $('.renew-package-list .loading').hide();
    }).error(function(data) {
        alert("Xảy ra lỗi!");
        $('.renew-package-list .loading').hide();
    });    
    
}
function UpAllProperty() {
    if ($('.uptimes').html() == null) {
        alert("Tính năng này chỉ dành cho gói UP tin.");
        return;
    }	    	    
    var url = "/publish/handler/Handler.ashx?command=" + USER_GET_RENEW_CAPTCHA;
    var id = $(this).parents('tr').find("#id").val();
    var row = $(this).parents('tr');
    //CHECK TO SHOW CAPTCHA
    $.post(url).success(function(data) {
        var fields = data.split(/;/);
        var type = fields[0];
        data = fields[1];
        if (type == 1) {//show captcha
            $('.list-property .captcha-box').hide(); //ẩn tất cả captcha
            $('.list-property .utils-action .captcha-box').html("<img class='captchagenerator' src='/CaptchaGenerator.ashx?t=" + Math.floor((Math.random() * 100000) + 1) + "' align='center'/><input type='text' class='captcha' maxlength='5'><input type='button' value='Xác thực' class='btncaptcha' />");
            $('.list-property .utils-action .captcha-box').show();
            $('.list-property .utils-action .captcha-box .captcha').focus();
        } else {
            //RENEW ALL
            var url = "/publish/handler/Handler.ashx?command=" + USER_RENEW_ALL_PROPERTY;
            $('.loading').show();
            $.post(url).success(function(data) {
                var fields = data.split(/;/);
                var success = fields[0];
                var fail = fields[1];
                //success
                alert("Làm mới thành công: " + success + ", thất bại: " + fail + ".");
                BackToListProperty(); //reload	        
                $('.loading').hide();
            }).error(function(data) {
                alert("Xảy ra lỗi!");
                $('.loading').hide();
            });
        }
    });
}

/*================== SHARE FORM ===========*/
$(document).ready(function() {
    $("body").on('click', '.sharemoneyform .search-box #btnSearchMember', function() {
        var receiver = $('.sharemoneyform .search-box #receiver').val();
        var url = "/publish/form/ShareMoneyForm.aspx?receiver=" + receiver;
        $('.loading').show();
        $.post(url).success(function(data) {
            $('.content-box').html(data);
            $('.loading').hide();
        }).error(function(data) {
            $('.loading').hide();
            alert("Xảy ra lỗi");
        });
    });

    $("body").on('click', '.sharemoneyform .search-box #btnSearchMember', function() {
        var receiver = $('.sharemoneyform .search-box #receiver').val();
        var url = "/publish/form/ShareMoneyForm.aspx?receiver=" + receiver;
        $('.loading').show();
        $.post(url).success(function(data) {
            $('.content-box').html(data);
            $('.loading').hide();
        }).error(function(data) {
            $('.loading').hide();
            alert("Xảy ra lỗi");
        });
    });

    $("body").on('keyup', '.sharemoneyform .inputform #tranferedamount', function() {
        TranferedAmountInput();
    });
    $("body").on('blur', '.sharemoneyform .inputform #tranferedamount', function() {
        TranferedAmountInput();
    })

    function TranferedAmountInput() {
        $('.sharemoneyform .message').hide();
        var tranferedamount = $('.sharemoneyform .inputform #tranferedamount');
        var number = tranferedamount.val();
        if (number == "")
            return;
        number = number.replace(/\s/g, "");
        number = number.replace(/\./g, ""); //remove.
        if (!isNumber(number)) {
            $('.sharemoneyform .message').html("- Số tiền nhập không hợp lệ");
            $('.sharemoneyform .message').show();
        }

        number = number.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        tranferedamount.val(number);

        var sodu = parseInt($('.sharemoneyform .inputform .sodu').html().replace(/\./g, ""));
        var sochuyen = tranferedamount.val().replace(/\./g, "");
        if (sochuyen == "" || !isNumber(sochuyen)) {
            $('.sharemoneyform .inputform .conlai').html("");
            return;
        }
        sochuyen = parseInt(sochuyen);
        var conlai = sodu - sochuyen;
        if (conlai < 0)
            $('.sharemoneyform .inputform .conlai').css('color', 'red');
        else
            $('.sharemoneyform .inputform .conlai').css('color', 'blue');
        $('.sharemoneyform .inputform .conlai').html(conlai.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
    }

    $("body").on('click', '.sharemoneyform .inputform #close', function() {
        BackToListProperty();
    });
    $("body").on('click', '.sharemoneyform .inputform #update', function() {
        $(".sharemoneyform .message").hide();
        var nguoinhan = $('.sharemoneyform .inputform #spnReceiver').html();
        var sotienchuyen = $('.sharemoneyform .inputform #tranferedamount').val().replace(/\./g, ""); //replace '.';
        var soconlai = $('.sharemoneyform .inputform #conlai').html().replace(/\./g, ""); //replace '.';
        var isValidate = true;
        var errorMessage = "";
        if (sotienchuyen == '' || !isNumber(sotienchuyen.replace(/-/g, ""))) { //replace '-'
            errorMessage += "<div>- Số tiền chuyển không hợp lệ. </div>";
            isValidate = false;
        }
        if (soconlai != '' && isNumber(soconlai.replace(/-/g, "")) && parseInt(soconlai) < 0)//nếu là số và lớn > 0
        {
            errorMessage += "<div>- Tài khoản không đủ tiền để thực hiện giao dịch. </div>";
            isValidate = false;
        }
        if (!isValidate) {
            $(".sharemoneyform .message").html(errorMessage);
            $(".sharemoneyform .message").show();
            return;
        }
        $('.loading').show();
        var url = "/publish/handler/Handler.ashx?command=" + USER_SHARE_MONEY;
        $.post(url, {
            nguoinhan: nguoinhan,
            sotienchuyen: sotienchuyen
        }).success(function(data) {
            var fields = data.split(/;/);
            var type = fields[0];
            var message = fields[1];
            if (type == 1)//success
            {
                $(".sharemoneyform .message").html("<span style='color:blue'>" + message + "</span>");
                $(".sharemoneyform .inputform").html("");//xóa dữ liệu cũ
            } else {
                $(".sharemoneyform .message").html(message);
            }
            $(".sharemoneyform .message").show();
            $('.loading').hide();
        }).error(function(data) {
            alert("Xảy ra lỗi!");
            $('.loading').hide();
        });
    });
});


/*========================= MESSENGER FORM ==============*/
$(document).ready(function() {
    var USER_MESSENGER_DETAIL = 20;
    $("body").on('click', '.messenger-list table .title', function() {

        if ($(this).hasClass('selected'))
            return;
        $('.loading').show();
        $('.messenger-list table .title').removeClass('selected');
        $('.messenger-list table .content').hide();
        $(this).addClass('selected');
        var id = $(this).parents("tr").find("#hiddenID").val();
        var content = $(this).parents("tr").find(".content");
        var url = "/publish/handler/Handler.ashx?command=" + USER_MESSENGER_DETAIL + "&id=" + id;
        $.post(url).success(function(data) {
            var fields = data.split(/;/);
            var type = fields[0];
            var message = fields[1];
            content.html(message);
            content.show();
            $('.loading').hide();
        }).error(function(data) {
            alert("Xảy ra lỗi!");
            $('.loading').hide();
        });
    });
});

//REPOST FORM
$(document).ready(function() {

    $("body").on('click', '.repostform .head', function(e) {

        $('.repostform .item .hinhthucdang').prop("checked", false);
        $('.repostform .item .over').show();
        $('.repostform .item').removeClass('selected');
        $('.repostform .item select').addClass('nonselected');

        $(this).parents('.item').addClass('selected');
        $(this).parents('.item').find('.over').hide();
        $(this).parents('.item').find('.hinhthucdang').prop("checked", true);
        $(this).parents('.item').find('select').removeClass('nonselected');

        var hinhthucdang = $(this).parents('.item').find('.hinhthucdang').val();
        if (hinhthucdang == 0) {
            $('.repostform .vipexplain').addClass('nonselected');
        } else {
            $('.repostform .vipexplain').removeClass('nonselected');
            LaySoDuTaiKhoan();
        }
    });

    $("body").on('change', '.repostform .vipperdayform .viptype', function(e) {
        var vipTypeId = $(this).val();

        if (vipTypeId == "") {
            $('.repostform .vipperdayform .chiphi').html("0");
            return;
        }
        vipTypeId = parseInt(vipTypeId);
        var dayNumber = parseInt($('.repostform .vipperdayform .daynumber').val());
        var price = parseInt($(".listviphidden #vip" + vipTypeId).attr("priceday"));
        //tính chi phí
        $('.repostform .vipperdayform .chiphi').html(FormatNumberWithDot(price * dayNumber));
    });

    $("body").on('change', '.repostform .vipperdayform .daynumber', function(e) {
        var vipTypeId = $('.repostform .vipperdayform .viptype').val();

        if (vipTypeId == "") {
            $('.repostform .vipperdayform .chiphi').html("0");
            return;
        }
        var dayNumber = parseInt($(this).val());
        vipTypeId = parseInt(vipTypeId);
        var price = parseInt($(".listviphidden #vip" + vipTypeId).attr("priceday"));
        //tính chi phí
        $('.repostform .vipperdayform .chiphi').html(FormatNumberWithDot(price * dayNumber));
    });

    //close repost form
    $("body").on('click', '.repostform .btnclose', function() {
        $('.content-box .repost-box').hide();
        $('.content-box .list-property-form').show();
    });

    $("body").on('click', '.repostform .btnrepost', function() {
        $('.content-box .repost-box .loading').show();
        var errorMessage = "";
        var hinhthucdang = $('.repostform *').removeClass('fielderror');
        var hinhthucdang = $('.repostform .hinhthucdang:checked').val();
        if (hinhthucdang == null) {
            ShowMemberPopup("Xảy ra lỗi", "Chưa chọn loại tin đăng");
            return;
        }
        var sodu = parseInt($('.repostform .sodutaikhoan .value').html().replace(/\./g, "")); //xoa dấm chấm
        var propertyid = $('.repostform .propertyid').val();
        var chiphi = 0;
        var vipTypeId = "";
        var dayNumber = "";
        if (hinhthucdang == 1) {
            vipTypeId = $('.repostform .vipperdayform .viptype').val();
            dayNumber = $('.repostform .vipperdayform .daynumber').val();
            if (vipTypeId == "") {
                $('.repostform .vipperdayform .viptype').addClass('fielderror');
                $('.repostform .vipperdayform .viptype').focus();
                errorMessage += "- Chưa chọn loại VIP.</br>";
            }
            if (dayNumber == "0") {
                $('.repostform .vipperdayform .daynumber').addClass('fielderror');
                $('.repostform .vipperdayform .daynumber').focus();
                errorMessage += "- Chưa chọn số ngày VIP.</br>";
            }
            if (errorMessage != "") {
                ShowMemberPopup("Xảy ra lỗi", errorMessage);
                return;
            }
            chiphi = parseInt($('.repostform .vipperdayform .chiphi').html().replace(/\./g, "")); //xoa dấm chấm;
        }
        if (hinhthucdang == 2) {
            if ($('.repostform .vippermonthform .viptype').val() == "") {
                $('.repostform .vippermonthform .viptype').addClass('fielderror');
                $('.repostform .vippermonthform .viptype').focus();
                errorMessage += "- Chưa chọn loại VIP.</br>";
            }
            if (errorMessage != "") {
                ShowMemberPopup("Xảy ra lỗi", errorMessage);
                return;
            }
            vipTypeId = $('.repostform .vippermonthform .viptype').val();
            chiphi = parseInt($(".repostform .listviphidden #vip" + vipTypeId).attr("pricemonth"));
        }
        if (sodu - chiphi < 0) {
            ShowMemberPopup("Xảy ra lỗi", "Không đủ số tiền để nâng cấp VIP");
            return;
        }
        var url = "/publish/handler/UpdateVipHandler.ashx?command=" + USER_REPOST;
        $.post(url, {
            propertyid: propertyid,
            hinhthucdang: hinhthucdang,
            viptypeid: vipTypeId,
            dayNumber: dayNumber
        }).success(function(data) {
            var fields = data.split(/;/);
            var type = fields[0];
            var message = fields[1];
            //success
            if (type == "1") {
                $('.repostform .inform-repost').show();
                $('.repostform .inform-repost .btninform').focus();
            } else {
                $('.repostform .errormessage').html(message).show(); //hiển thị thông báo lỗi        
            }
            $('.content-box .repost-box .loading').hide();
        }).error(function(data) {
            alert("Xảy ra lỗi!");
            $('.content-box .repost-box .loading').hide();
        });
    });

    $("body").on('click', '.repostform .btninform', function() {
        var propertyid = $(this).parents('.repostform').find('#propertyid').val();
        $('.content-box .repost-box').hide();
        $('.content-box .list-property-form .list-property table tr').find('#id[value=' + propertyid + ']').closest('tr').remove();
        $('.content-box .list-property-form').show();
    });
});
/*
//check allow re-post free or not
function CheckAllowRePost() {
    var matin = $('.post-content input#matin').val();
    if (matin == "") {
        var url = "/publish/handler/Handler.ashx?command=" + USER_CHECK_PROPERTY_INFORM_POPUP;
        $.post(url).success(function(data) {
            var notAllowPost = parseInt(data);
            if (notAllowPost == 1) {                
                //xử lý VIP box
                $('.repostfrom .vip-box .item .regular-property-over').show(); //disable regular property
                $(".repostfrom .vip-box .item").removeClass('selected');
                $(".repostfrom .vip-box .item .hinhthucdang[value='1']").prop("checked", true); //chọn VIP mặc định
                $(".repostfrom .vip-box .vipperdayform .over").hide();
                $(".repostfrom .vip-box .vipperdayform").addClass('selected');
                $('.repostfrom .vip-box .vipexplain').removeClass('nonselected');
                $(".repostfrom .vip-box .vipperdayform").find('select').removeClass('nonselected');
            };
        }).error(function(data) {

        });
    }
}
*/

$(document).ready(function() {
/*-- street acp --*/
    $("body").on('click', '.post-content .street-acp-box .street-row-down', function() {
        if ($('.post-content .street-acp-box ul').is(':hidden'))
            $('.post-content .street-acp-box input').trigger('focus');
        else
            $('.post-content .street-acp-box ul').hide();

    });
    $("body").on('click', '.post-content .street-acp-box input', function() {
        $('.post-content .street-acp-box input').trigger('focus');
    });

    $("body").on('focus', '.post-content .street-acp-box input', function() {
        $(this).closest('.value').find('.suggesstion').hide();
        $(".post-content .street-acp-box li").removeClass('hidden');
        $(".post-content .street-acp-box li").removeAttr('index');
        $(".post-content .street-acp-box ul").show();
    });

    $("body").on('keyup', '.post-content .street-acp-box input', function(event) {
        if (event.which == 13 || event.keyCode == 13) {
            UpdateSelectedStreet($('.post-content .street-acp-box li.selected'));
            return;
        }
        if (event.keyCode == 38) {
            if ($('.post-content .street-acp-box ul').is(':hidden'))
                return;
            var index = $('.post-content .street-acp-box li.selected').attr('index'); //index bắt đầu từ 0, nth-child bắt đầu từ 1
            if (index == null) {
                index = $('.post-content .street-acp-box li.selected').index();
                $(".post-content .street-acp-box li").removeClass('selected');
                $(".post-content .street-acp-box li:nth-child(" + index + ")").addClass('selected');
            } else {
                index = parseInt(index) - 1; //đi đến row trên
                if (index <= 0)
                    return;
                $(".post-content .street-acp-box li").removeClass('selected');
                $(".post-content .street-acp-box li[index='" + index + "'").addClass('selected');
            }
            return;
        }
        if (event.keyCode == 40) {
            if ($('.post-content .street-acp-box ul').is(':hidden'))
                return;
            //nếu chưa có li nào được chọn => chọn cái đầu tiên
            if (!$('.post-content .street-acp-box li').hasClass('selected')) {
                var index = $(".post-content .street-acp-box li[index='1'").attr('index');
                if (index == null)
                    $(".post-content .street-acp-box li:nth-child(1)").addClass('selected');
                else
                    $(".post-content .street-acp-box li[index='1'").addClass('selected');
            } else {

                var index = $('.post-content .street-acp-box li.selected').attr('index');
                if (index == null) {
                    index = $('.post-content .street-acp-box li.selected').index() + 2;
                    $(".post-content .street-acp-box li").removeClass('selected');
                    $(".post-content .street-acp-box li:nth-child(" + index + ")").addClass('selected');
                } else {
                    index = parseInt(index) + 1;
                    $(".post-content .street-acp-box li").removeClass('selected');
                    $(".post-content .street-acp-box li[index='" + index + "'").addClass('selected');
                }
            }
            return;
        }
        $(".post-content .street-acp-box li").removeAttr('index');
        $(".post-content .street-acp-box li").removeClass('selected');
        $(".post-content .street-acp-box li").removeClass('hidden');
        var keywords = $.trim($(this).val());
        keywords = keywords.replace(/  +/g, ' '); //xóa các khoảng trắng thừa
        var keywordsLink = locdau(keywords);
        //không tính ký tự đường phố
        if (keywords == "" || keywords.length < 3 || keywordsLink == "duong" || keywordsLink == "pho")
            return;
        //bỏ chữ đường | phố phía trước
        keywordsLink = keywordsLink.replace('duong-', '');
        keywordsLink = keywordsLink.replace('pho-', '');
        var count = 0;
        $(".post-content .street-acp-box li").each(function() {
            var streetName = $.trim($(this).html());
            var streetNameLength = streetName.length;
            var keywordsLength = keywords.length;

            if (keywordsLength < 3)
                return false;
            //từ khóa dài hơn tên đường => bỏ qua
            if (keywordsLength > streetNameLength) {
                $(this).addClass('hidden');
            } else {
                var linkStreet = locdau(streetName);
                if (linkStreet.indexOf(keywordsLink) == -1) {
                    $(this).addClass('hidden');
                } else {
                    count++;
                    $(this).attr('index', count);
                }
            }
        });
    });

    $("body").on('click', '.post-content .street-acp-box li', function() {
        UpdateSelectedStreet($(this));
    });

    $("body").on('click', '.post-content', function(e) {
        //return when street autocomplete is hidden
        if ($('.post-content .street-acp-box ul').is(':hidden'))
            return;

        var container = $(".post-content .street-acp-box");
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            //hide list street
            $(".post-content .street-acp-box ul").hide();

            //nếu đã từng chọn => chọn lại giá trị cũ
            if ($('.post-content .street-acp-box ul li').hasClass('selected')) {
                var name = $('.post-content .street-acp-box ul li.selected').html()
                var id = $('.post-content .street-acp-box ul li.selected').attr('value');
                if ($('.post-content .street-acp-box ul li.selected').attr('isfirst') == "1") {
                    id = "";
                }
                $(".post-content .street-acp-box input").val(name);
                $(".post-content .duong").val(id);
                return;
            }

            var keywords = $.trim($('.post-content .street-acp-box input').val());
            //have not keywords
            if (keywords.length == 0) {
                $('.post-content .duong').val("");
                $('.post-content .street-acp-box input').val("---- Đường/Phố ----");
            } else {
                var value = "";
                var name = "---- Đường/Phố ----";

                $(".post-content .street-acp-box li").each(function() {
                    if (!$(this).hasClass('hidden')) {
                        value = $(this).attr('value').toString();
                        name = $(this).html();
                        if ($(this).attr('isfirst') === "1")
                            value = "";
                        return false;
                    }
                });
                $('.post-content .duong').val(value);
                $('.post-content .street-acp-box input').val(name);
                UpdateMapByAddress(GetPropertyAddress());
            }
        };
    });

    //======== project-acp-box
    $("body").on('click', '.post-content .project-acp-box .project-row-down', function() {
    if ($('.post-content .project-acp-box ul').is(':hidden'))
        $('.post-content .project-acp-box input').trigger('focus');
        else
            $('.post-content .project-acp-box ul').hide();

    });
    $("body").on('click', '.post-content .project-acp-box input', function() {
    $('.post-content .project-acp-box input').trigger('focus');
    });

    $("body").on('focus', '.post-content .project-acp-box input', function() {
        $(this).closest('.value').find('.suggesstion').hide();
        $(".post-content .project-acp-box li").removeClass('hidden');
        $(".post-content .project-acp-box li").removeAttr('index');
        $(".post-content .project-acp-box ul").show();
    });

    $("body").on('keyup', '.post-content .project-acp-box input', function(event) {
        if (event.which == 13 || event.keyCode == 13) {
            UpdateSelectedProject($('.post-content .project-acp-box li.selected'));
            return;
        }
        if (event.keyCode == 38) {
            if ($('.post-content .project-acp-box ul').is(':hidden'))
                return;
            var index = $('.post-content .project-acp-box li.selected').attr('index'); //index bắt đầu từ 0, nth-child bắt đầu từ 1
            if (index == null) {
                index = $('.post-content .project-acp-box li.selected').index();
                $(".post-content .project-acp-box li").removeClass('selected');
                $(".post-content .project-acp-box li:nth-child(" + index + ")").addClass('selected');
            } else {
                index = parseInt(index) - 1; //đi đến row trên
                if (index <= 0)
                    return;
                $(".post-content .project-acp-box li").removeClass('selected');
                $(".post-content .project-acp-box li[index='" + index + "'").addClass('selected');
            }
            return;
        }
        if (event.keyCode == 40) {
            if ($('.post-content .project-acp-box ul').is(':hidden'))
                return;
            //nếu chưa có li nào được chọn => chọn cái đầu tiên
            if (!$('.post-content .project-acp-box li').hasClass('selected')) {
                var index = $(".post-content .project-acp-box li[index='1'").attr('index');
                if (index == null)
                    $(".post-content .project-acp-box li:nth-child(1)").addClass('selected');
                else
                    $(".post-content .project-acp-box li[index='1'").addClass('selected');
            } else {

            var index = $('.post-content .project-acp-box li.selected').attr('index');
                if (index == null) {
                    index = $('.post-content .project-acp-box li.selected').index() + 2;
                    $(".post-content .project-acp-box li").removeClass('selected');
                    $(".post-content .project-acp-box li:nth-child(" + index + ")").addClass('selected');
                } else {
                    index = parseInt(index) + 1;
                    $(".post-content .project-acp-box li").removeClass('selected');
                    $(".post-content .project-acp-box li[index='" + index + "'").addClass('selected');
                }
            }
            return;
        }
        $(".post-content .project-acp-box li").removeAttr('index');
        $(".post-content .project-acp-box li").removeClass('selected');
        $(".post-content .project-acp-box li").removeClass('hidden');
        var keywords = $.trim($(this).val());
        keywords = keywords.replace(/  +/g, ' '); //xóa các khoảng trắng thừa
        var keywordsLink = locdau(keywords);
        if (keywords == "" || keywords.length < 3)
            return;
        //bỏ chữ dự án phía trước
        keywordsLink = keywordsLink.replace('dua-an-', '');
        var count = 0;
        $(".post-content .project-acp-box li").each(function() {
            var projectName = $.trim($(this).html());
            var projectNameLength = projectName.length;
            var keywordsLength = keywords.length;

            if (keywordsLength < 3)
                return false;
            //từ khóa dài hơn tên dự án => bỏ qua
            if (keywordsLength > projectNameLength) {
                $(this).addClass('hidden');
            } else {
            var linkProject = locdau(projectName);
            if (linkProject.indexOf(keywordsLink) == -1) {
                    $(this).addClass('hidden');
                } else {
                    count++;
                    $(this).attr('index', count);
                }
            }
        });
    });

    $("body").on('click', '.post-content .project-acp-box li', function() {
        UpdateSelectedProject($(this));
    });

    $("body").on('click', '.post-content', function(e) {
        //return when street autocomplete is hidden
    if ($('.post-content .project-acp-box ul').is(':hidden'))
            return;

        var container = $(".post-content .project-acp-box");
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            //hide list street
            $(".post-content .project-acp-box ul").hide();

            //nếu đã từng chọn => chọn lại giá trị cũ
            if ($('.post-content .project-acp-box ul li').hasClass('selected')) {
                var name = $('.post-content .project-acp-box ul li.selected').html()
                var id = $('.post-content .project-acp-box ul li.selected').attr('value');
                if ($('.post-content .project-acp-box ul li.selected').attr('isfirst') == "1") {
                    id = "";
                }
                $(".post-content .project-acp-box input").val(name);
                $(".post-content #project").val(id);
                return;
            }

            var keywords = $.trim($('.post-content .project-acp-box input').val());
            //have not keywords
            if (keywords.length == 0) {
                $('.post-content #project').val("");
                $('.post-content .project-acp-box input').val("---- Chọn dự án ----");
            } else {
                var value = "";
                var name = "---- Chọn dự án ----";

                $(".post-content .project-acp-box li").each(function() {
                    if (!$(this).hasClass('hidden')) {
                        value = $(this).attr('value').toString();
                        name = $(this).html();
                        if ($(this).attr('isfirst') === "1")
                            value = "";
                        return false;
                    }
                });
                $('.post-content #project').val(value);
                $('.post-content .project-acp-box input').val(name);
            }
        };
    });
});
/*---end street acp ---*/
function InitStreetACPValue() {
    var data = "";
    $(".post-content .duong option").each(function() {
        // Add $(this).val() to your list
        var id = $(this).val();
        var name = $(this).text();
        if (id == "") {
            data += "<li value='" + id + "' isfirst='1'>" + name + "</li>";
        } else {
            if (id == "0")
                data += "<li style='color:blue' value='" + id + "'>" + name + "</li>";
            else
                data += "<li value='" + id + "'>" + name + "</li>";
        }
    });
    $(".post-content .street-acp-box ul").html(data);
    $('.post-content .street-acp-box input').val('---- Đường/Phố ----');
}
function UpdateSelectedStreet(row) {
    var id = row.attr('value');
    var name = row.html();
    if (row.attr('isfirst') == "1") {
        id = "";
    }
    $('.post-content .street-acp-box input').val(name);
    $('.post-content .duong').val(id);
    $(".post-content .street-acp-box ul").hide();
    row.closest('ul').find('li').removeClass('selected');
    row.addClass('selected');
    UpdateMapByAddress(GetPropertyAddress());
}
/*---end project acp ---*/
function InitProjectACPValue() {
    var data = "";
    $(".post-content #project option").each(function() {
        // Add $(this).val() to your list
        var id = $(this).val();
        var name = $(this).text();
        if (id == "") {
            data += "<li value='" + id + "' isfirst='1'>" + name + "</li>";
        } else {
            if (id == "0")
                data += "<li style='color:blue' value='" + id + "'>" + name + "</li>";
            else
                data += "<li value='" + id + "'>" + name + "</li>";
        }
    });
    $(".post-content .project-acp-box ul").html(data);
    $('.post-content .project-acp-box input').val('---- Chọ dự án ----');
}
function UpdateSelectedProject(row) {
    var id = row.attr('value');
    var name = row.html();
    if (row.attr('isfirst') == "1") {
        id = "";
    }
    $('.post-content .project-acp-box input').val(name);
    $('.post-content #project').val(id);
    $(".post-content .project-acp-box ul").hide();
    row.closest('ul').find('li').removeClass('selected');
    row.addClass('selected');
}
