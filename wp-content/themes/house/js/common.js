﻿var COMMAND_GET_DISTRICTS = 2;
var COMMAND_CHECK_LOGIN = 10;
var COMMAND_SET_WEB_VERSION = 24;
var COMMAND_GET_SAVED_PROPERTY_LIST = 28;
var COMMAND_GET_IGNORED_MEMBER_LIST = 29;
$(document).ready(function() {
    //go top
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('#to_top').fadeIn();
        }
        else {
            $('#to_top').fadeOut();
        }
        //history
        RePostionGuestConfig();
    });

    $('#to_top').click(function() {
        $('body,html').animate({ scrollTop: 0 }, 'slow');
    });
    RePostionGuestConfig();
    UpdateJustOnce();
    ShowGuestConfig();
});

function ViewWebVersion(version) {
    var url = "/handler/Handler.ashx?command=" + COMMAND_SET_WEB_VERSION + "&version=" + version;
    $.post(url).success(function(data) {
        var fields = data.split(/;/);
        var type = fields[0];
        //success
        if (type == "1") {
            location.reload();
        }
    }).error(function() {

    });
}
function Redirect(url) {
    window.parent.location = url;
}
function GoTop() {
    $('html, body').animate({ scrollTop: 0 }, 0);
}
function GoTop(offSetTop) {
    if (offSetTop == null)
        offSetTop = 0;
    $('html, body').animate({ scrollTop: offSetTop}, 0);
}
function isNumber(number) {
    for (var i = 0; i < number.length; i++) {
        var temp = number.substring(i, i + 1);
        if (!(temp >= "0" && temp <= "9")) {
            return false;
        }
    }
    return true;
}

function FormatNumberWithDot(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function ConvertNumberToText(price) {

    if (!isNumber(price))
        return "";

    price = price * 1000;
    var priceTy = parseInt(price / 1000000000, 0)
    var priceTrieu = parseInt((price % 1000000000) / 1000000, 0)
    var priceNgan = parseInt(((price % 1000000000)) % 1000000 / 1000, 0)
    var priceDong = parseInt(((price % 1000000000)) % 1000000 % 1000, 0)
    var strTextPrice = ""

    if (priceTy > 0 && parseInt(price, 0) > 900000000) {
        if (priceTrieu > 0) { getTrieu = "," + priceTrieu / 100; } else { getTrieu = ''; }
        strTextPrice = strTextPrice + priceTy + getTrieu + " Tỷ ";
    }
    if (priceTy == 0 && priceTrieu > 0) {
        if (priceNgan > 0) { getNgan = "," + priceNgan / 100; } else { getNgan = ''; }
        strTextPrice = strTextPrice + priceTrieu + getNgan + " Triệu ";
    }
    if (priceTrieu == 0 && priceNgan > 0) {
        if (priceDong > 0) { getDong = "," + priceDong / 100; } else { getDong = ''; }
        strTextPrice = strTextPrice + priceNgan + getDong + " Ngàn ";
    }
    if (priceNgan == 0 && priceDong > 0) {
        strTextPrice = strTextPrice + priceDong + " Đồng";
    }

    strTextPrice = strTextPrice.replace(/\./g, "");
    return strTextPrice;
}
function locdau(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/ /g, "-");
    str = str.replace(/,/g, "");
    return str;
}

function CheckLogin() {    
    var url = "/handler/Handler.ashx?command=" + COMMAND_CHECK_LOGIN;
    $.post(url, function(isLogin) {
        var isValidate = true;
        //success
        if (isLogin == "1") {
            location.href = "/dang-tin-nha-dat.html";
        } else {
            ShowLoginForm();
        }
    });
    return false;
}

function ShowLoginForm() {
    $('.login-form #account').val("");
    $('.login-form #password').val("");
    $('.login-form .input-form .message').html("");
    $('.login-form').show();
    $('.login-form #account').focus();
    $("#fade").show();
    GoTop(0);
}
function ShowRecoverForm() {
    $('.recover-form .input-form .message').html("");
    $('.recover-form #email').val("");
    $('.recover-form #username').val("");
    $('.recover-form').show();
    $('.recover-form #username').focus();
    $("#fade").show();
    GoTop(0);
}
function ShowRegisterForm() {
    $('.login-form').hide();
    var url = "/publish/form/RegisterForm.aspx";
    $.post(url, function(data) {
        $('.register-form').html(data);
        $('.register-form').show();
        $('.register-form #account').focus();
        $('#fade').show();
    });
}

function Register_LoadDistrict() {
    $('.register-form .loading').show();
    var province = $('.register-form #register_province').val();
    var url = "/handler/Handler.ashx?command=" + COMMAND_GET_DISTRICTS + "&matinh=" + province;
    $.post(url, function(data) {
        $('.register-form #register_district').html(data);
        $('.register-form .loading').hide();
    });
}
function Register_LoadDistrict1() {
    $('.register-form .loading').show();
    var province = $('.register-form #register_province1').val();
    var url = "/handler/Handler.ashx?command=" + COMMAND_GET_DISTRICTS + "&matinh=" + province;
    $.post(url, function(data) {
        $('.register-form #register_district1').html(data);
        $('.register-form .loading').hide();
    });
}
function DisableRegisterEmail() {
    if ($('.register-form .no-email').is(':checked')) {
        $('.register-form #email1').attr("disabled", "disabled");
        $('.register-form #email2').attr("disabled", "disabled");
    }
    else {
        $('.register-form #email1').removeAttr("disabled");
        $('.register-form #email2').removeAttr("disabled");
    }
}

function SaveProperty() {
    var propertyid = $('.property #hddPropertyId').val();        
    var listid = "";
    if (localStorage.getItem('savedproperty') != null && localStorage.getItem('savedproperty') != "") {
        //get history
        listid = localStorage.getItem('savedproperty');

        if (listid.indexOf(propertyid) >= 0)
            return;
            
        listid += "," + propertyid;

        //set
        localStorage.setItem('savedproperty', listid);
    } else {
        localStorage.setItem('savedproperty', propertyid);
    }
    ShowGuestConfig();
}
function ShowGuestConfig() {
    //localStorage.setItem('savedproperty', '');
    //load saved property
    var data = localStorage.getItem('savedproperty');
    if (data != null && data != "") {
        var listid = data.split(/,/);
        if (listid.length > 0) {
            if ($('.guest-config .head .saved-property-tab').length == 0) {
                $('.guest-config .head').prepend("<span class='saved-property-tab' onclick='ShowSavedPropertyBox()' title='Những tin mà bạn đã lưu'>Tin đã lưu (" + listid.length + ")</span>");
            } else {
                $('.guest-config .head .saved-property-tab').html("Tin đã lưu (" + listid.length + ")");
            }
        }
    }
    //localStorage.setItem('ignoredmember', '');
    data = localStorage.getItem('ignoredmember');
    if (data != null && data != "") {
        var lstIgnoredMember = data.split(/,/);
        if (lstIgnoredMember.length > 0) {
            if ($('.guest-config .head .ignored-member-tab').length == 0) {
                $('.guest-config .head').append("<span class='ignored-member-tab' onclick='ShowIgnoredMemberBox()' title='Những người đăng mà bạn không muốn xem tin'>Ẩn người đăng (" + lstIgnoredMember.length + ")</span>");
            } else {
            $('.guest-config .head .ignored-member-tab').html("Ẩn người đăng (" + lstIgnoredMember.length + ")");
            }
        }
    }

    //load ignored member
    if ($('.guest-config .head').html() != "") {
        $('.guest-config').show();
        RePostionGuestConfig();
    }
}
//update just once time per day
function UpdateJustOnce() {
    var flag = localStorage.getItem('uimflag');
    var currentDay = $('.hddCurrentTextDate').val();
    if (flag != currentDay) {
        localStorage.setItem('uimflag', currentDay);
        UpdateIgnoredCookies(); //update ignored member cookies
    }
    
}
function ShowSavedPropertyBox() {
    if (localStorage.getItem('savedproperty') == null || localStorage.getItem('savedproperty') == "")
        return;

    if ($('.saved-property-box').length == 0) {
        $('body').append("<div class='saved-property-box'><div class='head'>Tin đã lưu<span class='close' onclick='CloseSavedPropertyBox()'></span></div><div style='position:relative;'><div class='content'></div><div class='loading'></div></div></div>");
    }
    if ($('.saved-property-fade').length == 0) {
        $('body').append("<div class='saved-property-fade' onclick='CloseSavedPropertyBox()'></div>");
    }
    $('.saved-property-fade').show();
    $('.saved-property-box .loading').show();
    $('.saved-property-box').show();
    $('.saved-property-box').center('.saved-property-box');
    var listid = localStorage.getItem('savedproperty');

    var url = "/handler/Handler.ashx?command=" + COMMAND_GET_SAVED_PROPERTY_LIST;
    $.post(url, { listid: listid }).success(function(data) {
        if (data == "")
            $('.saved-property-box .content').html("Không có tin nào được lưu trữ");
        else
            $('.saved-property-box .content').html(data);
        $('.saved-property-box .loading').hide();
        $('.saved-property-box').center('.saved-property-box');
        GoTop($('.saved-property-box').offset().top - 50); //50 để không bị che bởi top contact
    }).error(function() {
        $('.saved-property-box .loading').hide();
    });
}

function CloseSavedPropertyBox() {
    $('.saved-property-box').remove();
    $('.saved-property-fade').remove();
}
function RemoveSavedProperty(item) {
    var propertyid = $(item).closest('.item').attr('value');
    $(item).closest('.item').remove();
    var count = 0;
    if (localStorage.getItem('savedproperty') != null) {
        var data = localStorage.getItem('savedproperty');
        var listid = data.split(/,/);
        var newList = "";

        for (var i = 0; i < listid.length; ++i) {
            var item = listid[i];
            if (listid[i] == propertyid) {
                continue;
            } else {
                if (newList == "")
                    newList = listid[i];
                else
                    newList += "," + listid[i];
                count++;
            }
        }
        localStorage.setItem('savedproperty', newList);
    }
    $('.guest-config .head .saved-property-tab').html("Tin đã lưu (" + count + ")");

    if (count == 0) {
        $('.guest-config .saved-property-tab').remove();
        $('.saved-property-box').remove();
        $('.saved-property-fade').remove();
    }
}

function ReflectSend() {
    var propertyid = $('.property #hddPropertyId').val();
    var reason = $('.reflect-form .reason-item:checked').val();
    var content = $('.reflect-form .txtcontent').val();
    var ignoredmember = 0;
    var dayignore = $('.reflect-form .dayignore').val();
    var captcha = $('.reflect-form .captcha').val();
    if ($('.reflect-form .chkdayignore').is(':checked'))
        ignoredmember = 1;

    if (content.length > 240) {
        alert("Nội dung quá dài, nội dung phải nhỏ hơn 240 ký tự.");
        return;
    }
    if (reason == null) {
        alert("Chưa chọn lý do");
        return;
    }
    if (ignoredmember == 1 && dayignore == 0) {
        alert("Chưa chọn số ngày không xem tin của thành viên này.");
        return;
    }
    if (captcha == "") {
        alert("Chưa nhập mã an toàn");
        return;
    }
    $('.reflect-form .loading').show();
    var url = "/handler/Handler.ashx?command=" + COMMAND_SEND_REFLECT_FORM;
    $.post(url, {
        propertyid: propertyid,
        reason: reason,
        content: content,
        ignoredmember: ignoredmember,
        dayignore: dayignore,
        captcha: captcha
    }, function(data) {
        var fields = data.split(/;/);
        var type = fields[0];
        var message = fields[1];
        if (type == 1) {
            //message chính là ngày tháng gửi phản ánh. Định dạng ddmmyy
            if (ignoredmember == 1) {
                AddIgnoredMember(message);
            }
            ResetFormReflect();
            alert("Xin cám ơn đã gửi phản ánh để chúng tôi phục vụ tốt hơn!");
        } else {
            alert(message);
        }
        $(".reflect-form .captchagenerator").attr('src', '/CaptchaGenerator.ashx?t=' + Math.floor((Math.random() * 100000) + 1));
        $('.reflect-form .loading').hide();
    });
}
function ResetFormReflect() {
    $('.reflect-form .reason-item').prop('checked', false);
    $('.reflect-form .txtcontent').val("");
    $('.reflect-form .dayignore').val(0);
    $('.reflect-form .captcha').val("");
    $('.reflect-form .chkdayignore').removeAttr('checked');
    $('.reflect-form .dayignore').attr("disabled", "disabled");
    $('.reflect-form .dayignore').css('background', '#eeeeee');
}

function AddIgnoredMember(expiredday) {
    //user of admin
    if ($('#hddNguoiDang').attr("user") == "1")
        return;
    var memberid = $('#hddNguoiDang').val();
    var item = memberid + "-" + expiredday;
    var lstIgnoredMember = localStorage.getItem('ignoredmember');
    //chưa tồn tại ai => thêm
    if (lstIgnoredMember == null || lstIgnoredMember == "") {
        localStorage.setItem('ignoredmember', item);
    } else {
        //check haven't this member is exist
        if (!IsExistInListIgnoredMember(memberid, lstIgnoredMember)) {
            lstIgnoredMember += "," + item;
            localStorage.setItem('ignoredmember', lstIgnoredMember);
        }
    }
    UpdateIgnoredCookies();
    ShowGuestConfig();
}
function RemoveIgnoredMember(item) {
    var memberid = $(item).closest('.item').attr('value');
    $(item).closest('.item').remove();
    var count = 0;
    if (localStorage.getItem('ignoredmember') != null && localStorage.getItem('ignoredmember') != "") {
        var data = localStorage.getItem('ignoredmember');
        var listid = data.split(/,/);
        var newList = "";

        for (var i = 0; i < listid.length; ++i) {
            var attr = listid[i].split(/-/);
            if (attr[0] == memberid) {
                continue;
            } else {
                if (newList == "")
                    newList = attr[0] + "-" + attr[1];
                else
                    newList += "," + attr[0] + "-" + attr[1];
                count++;
            }
        }
        localStorage.setItem('ignoredmember', newList);
    }
    $('.guest-config .head .ignored-member-tab').html("Ẩn người đăng (" + count + ")");

    if (count == 0) {
        $('.guest-config .ignored-member-tab').remove();
        $('.ignored-member-box').remove();
        $('.ignored-member-fade').remove();
    }
    UpdateIgnoredCookies();
}

function UpdateIgnoredCookies() {
    var data = localStorage.getItem('ignoredmember');
    if (data == null || data == "") {
        Cookies('ignoredmember', "", { expires: 365 });
        return;
    }
        
    var oldlstIgnoredMember = data.split(/,/);

    var txtDate = $(".hddCurrentTextDate").val();
    var lstCookieIgnoredMember = "";
    var newlstIgnoredMember = "";
    for (var i = 0; i < oldlstIgnoredMember.length; ++i) {
        var attr = oldlstIgnoredMember[i].split(/-/);
        if (attr.length != 2)//không đủ tham số
            continue;
        
        //tham số ko hợp lệ hoặc hết hạn
        if (!isNumber(attr[0]) || !isNumber(attr[1]) || attr[1] < txtDate)
            continue;
        //new cookie
        if(lstCookieIgnoredMember == "")
            lstCookieIgnoredMember = attr[0];
        else
            lstCookieIgnoredMember += "," + attr[0];
        
        //new localstorage ignored member
        if (newlstIgnoredMember == "")
            newlstIgnoredMember = attr[0] + "-" + attr[1];
        else
            newlstIgnoredMember += "," + attr[0] + "-" + attr[1];
    }
    //update new list
    if (newlstIgnoredMember != "" && lstCookieIgnoredMember != "") {
        Cookies('ignoredmember', lstCookieIgnoredMember, { expires: 365 });
        localStorage.setItem('ignoredmember', newlstIgnoredMember);
    } else 
    {
        Cookies('ignoredmember', '', { expires: 365 });
        localStorage.setItem('ignoredmember', '');
    }
}
function IsExistInListIgnoredMember(memberid, lstIgnoredMember) {
    var lst = lstIgnoredMember.split(/,/);
    for (var i = 0; i < lst.length; ++i) {
        var item = lst[i].split(/-/);
        if (item[0] == memberid)
            return true;
    }
    return false;
}
function ShowIgnoredMemberBox() {        
    if (Cookies('ignoredmember') == null || Cookies('ignoredmember') == "")
        return;

    if ($('.ignored-member-box').length == 0) {
        $('body').append("<div class='ignored-member-box'><div class='head'>Người đăng đã bị ẩn tin<span class='close' onclick='CloseIgnoredMemberBox()'></span></div><div style='position:relative;'><div class='content'></div><div><i>Tất cả các tin của những thành viên trong danh sách này sẽ bị ẩn đi với bạn.</i></div><div class='loading'></div></div></div>");
    }
    if ($('.ignored-member-fade').length == 0) {
        $('body').append("<div class='ignored-member-fade' onclick='CloseIgnoredMemberBox()'></div>");
    }
    $('.ignored-member-fade').show();
    $('.ignored-member-box .loading').show();
    $('.ignored-member-box').show();
    $('.ignored-member-box').center('.ignored-member-box');
    
    var listid = Cookies('ignoredmember');
    
    var url = "/handler/Handler.ashx?command=" + COMMAND_GET_IGNORED_MEMBER_LIST;
    $.post(url, { listid: listid }).success(function(data) {
        if (data == "")
            $('.ignored-member-box .content').html("Không có người đăng nào đang bị ẩn tin.");
        else
            $('.ignored-member-box .content').html(data);
        $('.ignored-member-box .loading').hide();
        $('.ignored-member-box').center('.ignored-member-box');
        GoTop($('.ignored-member-box').offset().top - 50); //50 để không bị che bởi top contact
    }).error(function() {
        $('.ignored-member-box .loading').hide();
    });
}
function CloseIgnoredMemberBox() {
    $('.ignored-member-box').remove();
    $('.ignored-member-fade').remove();
}
function ViewPropertyInMap(lat, lng) {
    if ($('#map').length == 0) {
        $('.property .image-tab').after('<div id="map"></div>');
        var geocoder;
        var map;
        var marker;
        var image = "/publish/img/home_map.gif";
        var image1 = "http://www.google.com/mapfiles/shadow50.png";
        var infowindow;
        geocoder = new google.maps.Geocoder();
        var defaultPosition = new google.maps.LatLng(lat, lng);
        var options = {
            scrollwheel: false,
            zoom: 16,
            center: defaultPosition,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: true,
            zoomControl: true
        };
        map = new google.maps.Map(document.getElementById('map'), options);

        var address;
        marker = new google.maps.Marker({
            map: map,
            position: defaultPosition,
            clickable: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggable: false,
            icon: image,
            shadow: image1
        });
        google.maps.event.addListener(marker, 'click', function() {
            geocoder.geocode({
                latLng: pos
            }, function(responses) {
                if (responses && responses.length > 0) {
                    infowindow.setContent("<span id='address'><b>Địa chỉ : </b>" + responses[0].formatted_address + "</span>");
                    infowindow.open(map, marker);
                } else {
                    infowindow.setContent("<span id='address'><b>Địa chỉ : </b> Cannot determine address at this location.</span>");
                    infowindow.open(map, marker);
                }
            });
        });
    }
}
function ViewPropertyInFrame(lat, lng) {
    if ($('#map').length == 0) {
        $('.property .image-tab').after("<div id='map'><iframe title='Hồ Chí Minh' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com/maps?&amp;q=+"+lat+",+"+lng+"&amp;output=embed'></iframe></div>");
    }
}
