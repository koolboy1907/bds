﻿// JavaScript Document
var geocoder;
var map;
var marker;
var image = "/publish/img/home_map.gif";
var image1 = "http://www.google.com/mapfiles/shadow50.png";
var infowindow;
var defLat = 10.810583;
var defLng = 106.70914;
function InitMap(latitude, longitude) {
	//hide map if browser is firefox
    if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
        $('.bando').hide();
        return;
    }
    if (latitude != 0) {
        defLat = latitude;
        defLng = longitude;
    }
    geocoder = new google.maps.Geocoder();
    var defaultPosition = new google.maps.LatLng(defLat, defLng);
    var options = {
        scrollwheel: false,
        zoom: 16,
        center: defaultPosition,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        panControl: true,
        zoomControl: true
    };
    map = new google.maps.Map(document.getElementById('map'), options);

    var address;
    marker = new google.maps.Marker({
        map: map,
        position: defaultPosition,
        clickable: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        draggable: true,
        icon: image,
        shadow: image1
    });

    infowindow = new google.maps.InfoWindow({
        content: "Hồ Chí Minh"
    }); //end InfoWindow

    google.maps.event.addListener(marker, 'click', function() {
        UpdateMarkerPosition(marker);
        showAddress(marker.getPosition());
    });
    google.maps.event.addListener(marker, 'drag', function() {
        UpdateMarkerPosition(marker);
        showAddress(marker.getPosition());
    });
    google.maps.event.addListener(marker, 'dragen', function() {
        UpdateMarkerPosition(marker);
        showAddress(marker.getPosition());
    });
    if ($('.post-content #matin').val() != "" && (parseInt(latitude) == 0 && parseInt(longitude) == 0)) {
        UpdateMapByAddress(GetPropertyAddress());
    }
    showAddress(marker.getPosition());
}

function UpdateMapByAddressAfterLoadPage(latitude, longitude) {
    
}
function GetPropertyAddress() {
    var noHouse = "";
    var street = "";
    var ward = "";
    var district = "";
    var province = "";
    var address = "";
    if ($('.post-content .diachi').val() != "") {
        address = $(".post-content .diachi").val() + ", ";
    }
    if ($('.post-content .duong').val() != "" && $('.post-content .duong').val() != "0") {
        address += $(".post-content .duong option:selected").text() + ", ";
    } else {
        if ($('.post-content .phuong').val() != "" && $('.post-content .phuong').val() != "0") {
            address += $(".post-content .phuong option:selected").text() + ", ";
        } 
    }
    if ($('.post-content .huyen').val() != "" && $('.post-content .huyen').val() != "0") {
        address += $(".post-content .huyen option:selected").text() + ", ";
    }
    if ($('.post-content .tinh').val() != "" && $('.post-content .tinh').val() != "0") {
        address += $(".post-content .tinh option:selected").text();
    }
    return address;
}
function UpdateMapByAddress(address) {
    if (map == null)
        return;
    if (address.length == 0)
        return;

    if (geocoder == null) {
        geocoder = new google.maps.Geocoder();
    }

    geocoder.geocode(
    { 'address': address },
    function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var pos = results[0].geometry.location;
            map.setCenter(pos);

            if (marker == null) {
                marker = new google.maps.Marker({
                    map: map,
                    position: pos,
                    draggable: true
                });
            }

            marker.setPosition(pos);
            UpdateMarkerPosition(marker);
            showAddress(pos);
            infowindow.close();
        }
    });
}

function UpdateMarkerPosition(curMarker) {    
    $("#map_lat").val(curMarker.getPosition().lat());
    $("#map_lng").val(curMarker.getPosition().lng());
}

function showAddress(pos) {
    geocoder.geocode({
        latLng: pos
    }, function(responses) {
        if (responses && responses.length > 0) {
            infowindow.setContent("<span id='address'><b>Vị trí gần đúng : </b>" + responses[0].formatted_address + "</span>");
            infowindow.open(map, marker);
        } else {
            //infowindow.setContent("<span id='address'><b>Địa chỉ : </b> Không xác định được tên đường.</span>");
        //infowindow.open(map, marker);
        //infowindow.close();
        }
    });
}
