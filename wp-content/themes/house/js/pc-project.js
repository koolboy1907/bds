﻿var COMMAND_PROJECT_AUTO_COMPLETE = 21;
$(document).ready(function() {
    //runbanner(670, 250);  

    $("body").on('click', '.project-menu-tab li', function(event) {
        $('.project-menu-tab li').removeClass('current');
        $(this).addClass('current');
        var index = $(this).index();
        $('.project-content .item').hide();
        $('.project-content .item').eq(index).show();

        var content = $('.project-content .item').eq(index).html();
        content = content.replace(/_thumbs\//g, "");
        $('.project-content .item').eq(index).html(content);
    });
    $("body").on('click', '.project-content .item img', function(event) {
        $("body .image-popup").remove();//xóa nếu tồn tại
        $("body").append("<div class='image-popup'><span class='exit' onclick='CloseImagePopup()'></span><div class='image'></div></div>");

        var popup = $(".image-popup");
        var copyedImage = $(this).clone();
        //var imageWidth = 0;
        var img = new Image();
        $(img).load(function() {
            //imageWidth = img.width;

            popup.show().css("top", ($(window).height()) / 2 + $(window).scrollTop() - 300 + "px");
            if ($(window).width() > img.width)
                popup.css("left", (($(window).width() / 2) - (img.width / 2)) + "px");
            else
                popup.css("left", "0px");
            popup.find('.image').html(copyedImage);
            popup.find('.image img').css('width', img.width + 'px');
            popup.find('.image img').css('height', img.height + 'px');
        });
        img.src = copyedImage.attr('src');
    });

    $('.project-search #projectname').autocomplete({
        serviceUrl: '/handler/Handler.ashx?command=' + COMMAND_PROJECT_AUTO_COMPLETE,
        minChars: 3,
        lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
            var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
            return re.test(suggestion.value);
        },
        onSelect: function(suggestion) {
            location.href = "/du-an-aaa-pj" + suggestion.data; //tự điều hướng đúng url
        }
    });
});


function CloseImagePopup() {
    $(".image-popup").remove();
    $('#fade').hide();
}

function ShowImage() {
    
}
function PJGetDistricts() {
    var province = $('.province').val();
    var url = "/handler/Handler.ashx?command=2&matinh=" + province;
    $.post(url, function(data) {
        $('.district').html(data);
    });
}

function PJDistrictClick() {
    var items = $('.district option').length;
    var province = $('.province').val();
    if (province != 0 && items < 2) {
        PJGetDistricts();
    }
}

function SearchProject() {
    var province = $('.province').val();
    var district = $('.district').val();
    var projectcategory = $('.projectcategory').val();
    if (district == null)
        return;
    var provincename = $('.province :selected').text();
    var districtname = $('.district :selected').text();
    var projectcategoryname = $('.projectcategory :selected').text();
    provincename = locdau(provincename);
    districtname = locdau(districtname);
    projectcategoryname = locdau(projectcategoryname);
    var url = "";
    if (projectcategory == 0) 
    {        
        if (province == 0 && district == 0) {
            url = "/du-an-bat-dong-san";
        } else {
        if (district != 0)
            url = "/du-an-bat-dong-san-" + districtname + "-qh" + district;

        else
            url = "/du-an-bat-dong-san-" + provincename + "-pv" + province;
        } 
    } else 
    {
        if (province == 0 && district == 0) {
            url = "/du-an-" + projectcategoryname + "-pc" + projectcategory;
        } else {
            if (district != 0)
                url = "/du-an-" + projectcategoryname + "-" + districtname + "-pc" + projectcategory + "-qh" + district;                
            else
                url = "/du-an-" + projectcategoryname + "-" + provincename + "-pc" + projectcategory + "-pv" + province;                
        }
        
     }
    window.parent.location = url;
}